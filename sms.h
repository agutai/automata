#ifndef SMS_H
#define SMS_H

int check_sms_comm(int len);
void sendSMSmsg(void);
void fill_sensor_data(unsigned char *buff);

void clear_sensor_data(void);
void set_sensor_data(int idx, unsigned char *data);

extern unsigned char sms_rx_buff[];
extern unsigned char sms_tx_buff[];


#endif // SMS_H
