#include "AT91SAM7X256.H"
#include "CAN_data.h"
#include "Board.h"
#include "expander.h"
#include "data.h"

unsigned char CANRxdata50[20];
unsigned char CANRxdata70[20];
unsigned char CANTxdata70[20];
unsigned char CANRxdata71[20];
unsigned char CANRxdata80[20];
unsigned char CANTxdata[20];


void setCANrx_data(int ID, unsigned char buff[])
{
	int i;
	switch (ID)
	{
		case 50:
			for(i=0;i<8;i++){CANRxdata50[i] = buff[i];} break;

		case 70:
			for(i=0;i<8;i++){CANRxdata70[i] = buff[i];} break;
			
		case 80:
			for(i=0;i<8;i++){CANRxdata80[i] = buff[i];} break;
	}
} 

void setCANtx_data(int page, unsigned char buff[])
{
	int i, start;

	if(page == 0x00){start = 0;}
	if(page == 0X01){start = 7;}
	if(page == 0X02){start = 14;}

	buff[0] = page;
	for(i=0;i<7;i++){buff[i+1] = CANTxdata[i+start];}
}

void setCAN70tx_data(int page, unsigned char buff[])
{
	int i, start;

	if(page == 0x00){start = 0;}
	if(page == 0X01){start = 7;}
	if(page == 0X02){start = 14;}

	buff[0] = page;
	for(i=0;i<7;i++){buff[i+1] = CANTxdata70[i+start];}
}


void refreshCANdata(void)
{
	int i;

	i=getHomero1();
	CANTxdata[4] = i>>8;
	CANTxdata[5] = i;

	i=getHomero2();
	CANTxdata[6] = i>>8;
	CANTxdata[7] = i;

	i=getHofok1();
	CANTxdata[8] = i>>8;
	CANTxdata[9] = i;

	i=getImitActIdo();
	CANTxdata[11] = i>>8;
	CANTxdata[12] = i;

	CANTxdata[10] = getRostelyseb();


	if(getKezi())				{CANTxdata[1] |= (1<<0);} else{CANTxdata[1] &= ~(1 << 0);}
	if(TARTALY_URES)			{CANTxdata[1] |= (1<<1);} else{CANTxdata[1] &= ~(1 << 1);}
	if(TARTALY_TELE)			{CANTxdata[1] |= (1<<2);} else{CANTxdata[1] &= ~(1 << 2);}
	if(AJTO_NYITVA)				{CANTxdata[1] |= (1<<3);} else{CANTxdata[1] &= ~(1 << 3);}
	if(AJTO_CSUKVA)				{CANTxdata[1] |= (1<<4);} else{CANTxdata[1] &= ~(1 << 4);}
	if(getToltorendszerTiltas())		{CANTxdata[1] |= (1<<5);} else{CANTxdata[1] &= ~(1 << 5);}
	if(getSalakolorendszerTiltas())		{CANTxdata[1] |= (1<<6);} else{CANTxdata[1] &= ~(1 << 6);}
}


//�ppen t�lt
void setCANTolt(void)
{
 	CANTxdata[1] |= (1 << 7);
}

void clearCANTolt(void)
{
	CANTxdata[1] &= ~(1 << 7);
}

int getCANTolt(void)
{
	if((CANTxdata[1] & (1<< 7)) > 0)return 1;
	else return 0;
}

//�ppen salakol
void setCANSalakol(void)
{
 	CANTxdata[2] |= (1 << 0);
}

void clearCANSalakol(void)
{
	CANTxdata[2] &= ~(1 << 0);
}

int getCANSalakol(void)
{
	if((CANTxdata[2] & (1<< 0)) > 0)return 1;
	else return 0;
}



//�kl�tra mehet
void setCANEkletraMehet(void)
{
 	CANTxdata[2] |= (1 << 1);
}

void clearCANEkletraMehet(void)
{
	CANTxdata[2] &= ~(1 << 1);
}

int getCANEkletraMehet(void)
{
	if((CANTxdata[2] & (1<< 1)) > 0)return 1;
	else return 0;
}

//Gy�jt�csiga
void setCANGyujtoMehet(void)
{
 	CANTxdata[2] |= (1 << 2);
}

void clearCANGyujtoMehet(void)
{
	CANTxdata[2] &= ~(1 << 2);
}

int getCANGyujtoMehet(void)
{
	if((CANTxdata[2] & (1<< 2)) > 0)return 1;
	else return 0;
}

//K�ls� salakol�
void setCANSalakoloMehet(void)
{
 	CANTxdata[2] |= (1 << 3);
}

void clearCANSalakoloMehet(void)
{
	CANTxdata[2] &= ~(1 << 3);
}

int getCANSalakoloMehet(void)
{
	if((CANTxdata[2] & (1<< 3)) > 0)return 1;
	else return 0;
}






//Hiba�zenetek
void setCANAramszunet(void)
{
 	CANTxdata[3] |= (1 << 0);
}

void clearCANAramszunet(void)
{
	CANTxdata[3] &= ~(1 << 0);
}

int getCANAramszunet(void)
{
	if((CANTxdata[3] & (1<< 0)) > 0)return 1;
	else return 0;
}
////////////

void setCANVesztermosztat(void)
{
 	CANTxdata[3] |= (1 << 1);
}

void clearCANVesztermosztat(void)
{
	CANTxdata[3] &= ~(1 << 1);
}

int getCANVesztermosztat(void)
{
	if((CANTxdata[3] & (1<< 1)) > 0)return 1;
	else return 0;
}
////////////
void setCANKazanKihult(void)
{
 	CANTxdata[3] |= (1 << 2);
}

void clearCANKazanKihult(void)
{
	CANTxdata[3] &= ~(1 << 2);
}

int getCANKazanKihult(void)
{
	if((CANTxdata[3] & (1<< 2)) > 0)return 1;
	else return 0;
}
////////////

void setCANVizszintAlacsony(void)
{
 	CANTxdata[3] |= (1 << 3);
}

void clearCANVizszintAlacsony(void)
{
	CANTxdata[3] &= ~(1 << 3);
}

int getCANVizszintAlacsony(void)
{
	if((CANTxdata[3] & (1<< 3)) > 0)return 1;
	else return 0;
}
////////////

void setCANToltoHiba(void)
{
 	CANTxdata[3] |= (1 << 5);
}

void clearCANToltoHiba(void)
{
	CANTxdata[3] &= ~(1 << 5);
}

int getCANToltoHiba(void)
{
	if((CANTxdata[3] & (1<< 5)) > 0)return 1;
	else return 0;
}

void setCANToltoajtoNemzart(void)
{
 	CANTxdata[3] |= (1 << 6);
}

void clearCANToltoajtoNemzart(void)
{
	CANTxdata[3] &= ~(1 << 6);
}

int getCANToltoajtoNemzart(void)
{
	if((CANTxdata[3] & (1<< 6)) > 0)return 1;
	else return 0;
}

void setCANSalakoloHiba(void)
{
 	CANTxdata[3] |= (1 << 7);
}

void clearCANSalakoloHiba(void)
{
	CANTxdata[3] &= ~(1 << 7);
}

int getCANSalakoloHiba(void)
{
	if((CANTxdata[3] & (1<< 7)) > 0)return 1;
	else return 0;
}



int getCANHidraulikaNyomas(void)
{
 	return ((CANRxdata80[0]<<8) + CANRxdata80[1]);
}

//�kl�tra �zenetek
int getCANSalakoloOK(void)
{
	int i = getKazanNum();

	if((CANRxdata50[5] & (1<< i)) > 0)return 1;
	else return 0;
}

//�kl�tra hiba�zenetek

int getCANEkletraErrorCsiga1Elakadt(void)
{
	if((CANRxdata50[0] & (1<< 1)) > 0)return 1;
	else return 0;
}

int getCANEkletraErrorElakadt(void)
{
	if((CANRxdata50[0] & (1<< 7)) > 0)return 1;
	else return 0;
}

int getCANEkletraErrorIdotullepes(void)
{
	if((CANRxdata50[1] & (1<< 0)) > 0)return 1;
	else return 0;
}

int getCANKulsoSalakoloError(void)
{
	if((CANRxdata50[1] & (1<< 6)) > 0)return 1;
	else return 0;
}





//----------ID 70 �kl�tra

//�kl�tra mehet
void setCAN70EkletraMehet(void)
{
 	CANTxdata70[2] |= (1 << 0);
}

void clearCAN70EkletraMehet(void)
{
	CANTxdata70[2] &= ~(1 << 0);
}

int getCAN70EkletraMehet(void)
{
	if((CANTxdata70[2] & (1<< 0)) > 0)return 1;
	else return 0;
}

//Gy�jt�csiga
void setCAN70GyujtocsigaMehet(void)
{
 	CANTxdata70[2] |= (1 << 1);
}

void clearCAN70GyujtocsigaMehet(void)
{
	CANTxdata70[2] &= ~(1 << 1);
}

int getCAN70GyujtocsigaMehet(void)
{
	if((CANTxdata70[2] & (1<< 1)) > 0)return 1;
	else return 0;
}

//�kl�tra tiltas
void setCAN70EkletraTilt(void)
{
 	CANTxdata70[2] |= (1 << 2);
}

void clearCAN70EkletraTilt(void)
{
	CANTxdata70[2] &= ~(1 << 2);
}

int getCAN70EkletraTilt(void)
{
	if((CANTxdata70[2] & (1<< 2)) > 0)return 1;
	else return 0;
}

//Csiga tiltas
void setCAN70Csiga1Tilt(void)
{
 	CANTxdata70[2] |= (1 << 3);
}

void clearCAN70Csiga1Tilt(void)
{
	CANTxdata70[2] &= ~(1 << 3);
}

int getCAN70Csiga1Tilt(void)
{
	if((CANTxdata70[2] & (1<< 3)) > 0)return 1;
	else return 0;
}


//�kl�tra adatok

void setCAN70ElakadasiNyomas(int i){CANTxdata70[3] = i;}

void setCAN70ElakadasiIdotullepes(int i){CANTxdata70[4] = i;}

void setCAN70Csiga1Forgido(int i){CANTxdata70[5] = i;}

void setCAN70Csiga2Forgido(int i){CANTxdata70[6] = i;}

void setCAN70Probalkozasszam(int i){CANTxdata70[7] = i;}



////// �kl�tra gombok

void setCANbuttonKezi(){CANTxdata70[1] |= (1 << 0);}
void clearCANbuttonKezi(){CANTxdata70[1] &= ~(1 << 0);}

void setCANbuttonHidrotap(){CANTxdata70[1] |= (1 << 1);}
void clearCANbuttonHidrotap(){CANTxdata70[1] &= ~(1 << 1);}

void setCANbuttonEkletraElore(){CANTxdata70[1] |= (1 << 2);}
void clearCANbuttonEkletraElore(){CANTxdata70[1] &= ~(1 << 2);}

void setCANbuttonEkletraHatra(){CANTxdata70[1] |= (1 << 3);}
void clearCANbuttonEkletraHatra(){CANTxdata70[1] &= ~(1 << 3);}

void setCANbuttonCsiga1Elore(){CANTxdata70[1] |= (1 << 4);}
void clearCANbuttonCsiga1Elore(){CANTxdata70[1] &= ~(1 << 4);}

void setCANbuttonCsiga1Hatra(){CANTxdata70[1] |= (1 << 5);}
void clearCANbuttonCsiga1Hatra(){CANTxdata70[1] &= ~(1 << 5);}

void setCANbuttonCsiga2Elore(){CANTxdata70[1] |= (1 << 6);}
void clearCANbuttonCsiga2Elore(){CANTxdata70[1] &= ~(1 << 6);}

void setCANbuttonCsiga2Hatra(){CANTxdata70[1] |= (1 << 7);}
void clearCANbuttonCsiga2Hatra(){CANTxdata70[1] &= ~(1 << 7);}

void setCANbuttonClear(){CANTxdata70[2] |= (1 << 7);}
void clearCANbuttonClear(){CANTxdata70[2] &= ~(1 << 7);}


//////////////////////////


//�kl�tra ID70

//Bemenetek

int getCAN70EkletraSzint(void)
{
	if((CANRxdata70[1] & (1<< 5)) > 0)return 1;
	else return 0;
}

int getCAN70CsigaSzint(void)
{
	if((CANRxdata70[1] & (1<< 6)) > 0)return 1;
	else return 0;
}

//Kimenetek
int getCAN70EkletraEloreMegy(void)
{
	if((CANRxdata70[3] & (1<< 1)) > 0)return 1;
	else return 0;
}

int getCAN70EkletraHatraMegy(void)
{
	if((CANRxdata70[3] & (1<< 2)) > 0)return 1;
	else return 0;
}

//Hiba�zenetek

int getCAN70error(void)
{
	return CANRxdata70[5];
}

int getCAN70errorEkletraIdotullepes(void)
{
	if((CANRxdata70[5] & (1<< 0)) > 0)return 1;
	else return 0;
}

int getCAN70errorCsiga1Probal(void)
{
	if((CANRxdata70[5] & (1<< 1)) > 0)return 1;
	else return 0;
}

int getCAN70errorCsiga1Elakadt(void)
{
	if((CANRxdata70[5] & (1<< 2)) > 0)return 1;
	else return 0;
}

int getCAN70BoilerStop(void) // A kaz�nh�zi vez�rl� k�ldi a kaz�nnak hogy �llj
{
	if((CANRxdata70[6] & (1<< 7)) > 0)return 1;
	else return 0;
}

//Bemenetek
int getCAN70inputs(void)
{
 	return CANRxdata70[1];
}

int getCAN70szivattyuMV(void)
{
	if((CANRxdata70[1] & (1<< 0)) > 0)return 1;
	else return 0;
}

int getCAN70csiga1MV(void)
{
	if((CANRxdata70[1] & (1<< 1)) > 0)return 1;
	else return 0;
}

int getCAN70csiga2MV(void)
{
	if((CANRxdata70[1] & (1<< 2)) > 0)return 1;
	else return 0;
}

int getCAN70ekletraElol(void)
{
	if((CANRxdata70[1] & (1<< 3)) > 0)return 1;
	else return 0;
}

int getCAN70ekletraHatul(void)
{
	if((CANRxdata70[1] & (1<< 4)) > 0)return 1;
	else return 0;
}

int getCAN70ekletraSzint(void)
{
	if((CANRxdata70[1] & (1<< 5)) > 0)return 1;
	else return 0;
}

int getCAN70csiga1Szint(void)
{
	if((CANRxdata70[1] & (1<< 6)) > 0)return 1;
	else return 0;
}

int getCAN70csiga2Szint(void)
{
	if((CANRxdata70[1] & (1<< 7)) > 0)return 1;
	else return 0;
}


//Kimenetek

int getCAN70outputs(void)
{
 	return CANRxdata70[3];
}

int getCAN70ekletraEloreMegy(void)
{
	if((CANRxdata70[3] & (1<< 1)) > 0)return 1;
	else return 0;
}

int getCAN70ekletraHatraMegy(void)
{
	if((CANRxdata70[3] & (1<< 2)) > 0)return 1;
	else return 0;
}

//Refresh
void refreshCAN70data(void)
{
	setCAN70ElakadasiNyomas(getEkletraElakadasinyomas());
	setCAN70ElakadasiIdotullepes(getEkletraIdotullepes());
	setCAN70Csiga1Forgido(getGyujto1Forgido());
	setCAN70Probalkozasszam(getProbalkozasSzam());

	if(getEkletraTiltas()){setCAN70EkletraTilt();}
	else{clearCAN70EkletraTilt();}


	if(getEkletraKeziGomb()){setCANbuttonKezi();}
	else{clearCANbuttonKezi();}

	if(getHidrotapGomb()){setCANbuttonHidrotap();}
	else{clearCANbuttonHidrotap();}

	if(getEkletraEloreGomb()){setCANbuttonEkletraElore();}
	else{clearCANbuttonEkletraElore();}

	if(getEkletraHatraGomb()){setCANbuttonEkletraHatra();}
	else{clearCANbuttonEkletraHatra();}

	if(getGyujtocsigaEloreGomb()){setCANbuttonCsiga1Elore();}
	else{clearCANbuttonCsiga1Elore();}

	if(getGyujtocsigaHatraGomb()){setCANbuttonCsiga1Hatra();}
	else{clearCANbuttonCsiga1Hatra();}

	if(getClearGomb()){	setCANbuttonClear();}
	else{clearCANbuttonClear();}

}

int getCAN71BoilerNum(void)
{
	return CANRxdata71[0];
}

int getCAN71SetBit(void)
{
	return CANRxdata71[1] & 0x01;
}

int getCAN71StopBit(void)
{
	return CANRxdata71[1] & 0x02;
}

int getCAN71PresetTemp(void)
{
	return (CANRxdata71[2] << 8) | CANRxdata71[3];
}

int getCAN71PresetBar(void)
{
	return CANRxdata71[2];
}
