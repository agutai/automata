
//***************************************************************************
//***************************************************************************
//			CG 1.3.5 panel					**
//									 **
//									 **
//									 **
//									 **
//***************************************************************************
//***************************************************************************

//#define CARBOKAZAN


#ifndef __BOARD_H
#define __BOARD_H

#include "AT91SAM7X256.H"

#define true	1
#define false	0


#define BOLYG_ON		pwm0_on(); setOutputPin(37,0); BOLYG230_ON;
#define BOLYG_OFF		pwm0_off(); clearOutputPin(37,0); BOLYG230_OFF;

#define KULSO_TOLTO_ON 		set_outputs(11);
#define KULSO_TOLTO_OFF 	clear_outputs(11);

#define SZIVATTYU_TILTAS_ON 	set_outputs(10);
#define SZIVATTYU_TILTAS_OFF 	clear_outputs(10);

#define TUZOLTAS_ON		set_outputs(12);
#define TUZOLTAS_OFF		clear_outputs(12);

#define RELA_ON			set_outputs(9);
#define RELA_OFF		clear_outputs(9); 
#define RELB_ON			set_outputs(8);
#define RELB_OFF		clear_outputs(8);

#define ROSTELY_ON		set_outputs(6);	setOutputPin(36,0);
#define ROSTELY_OFF		clear_outputs(6); clearOutputPin(36,0);

#define VENTI_ON 		set_outputs(5);	setOutputPin(36,1);
#define VENTI_OFF 		clear_outputs(5); clearOutputPin(36,1);

#define BELIMO_ON 		set_outputs(4); setOutputPin(36,2);
#define BELIMO_OFF 		clear_outputs(4); clearOutputPin(36,2);

#define OUT7_ON 		set_outputs(3);
#define OUT7_OFF 		clear_outputs(3);

#define SZIVATTYU_ON 		set_outputs(2);	setOutputPin(36,4);
#define SZIVATTYU_OFF 		clear_outputs(2); clearOutputPin(36,4);

#define BOLYG230_ON 		set_outputs(1);
#define BOLYG230_OFF 		clear_outputs(1);

#define SZIVATTYU2_ON 		set_outputs(0);
#define SZIVATTYU2_OFF 		clear_outputs(0);

#define RAZOMOTOR_ON 		set_outputs(14);
#define RAZOMOTOR_OFF 		clear_outputs(14);

#define BELL_ON 		AT91C_BASE_PIOA->PIO_SODR = (1<<8)
#define BELL_OFF 		AT91C_BASE_PIOA->PIO_CODR = (1<<8)

#define ERROR_LED_ON 		AT91C_BASE_PIOB->PIO_SODR = (1<<30)
#define ERROR_LED_OFF 		AT91C_BASE_PIOB->PIO_CODR = (1<<30)

#define RS485_ENA 		AT91C_BASE_PIOB->PIO_SODR = (1<<28)
#define RS485_DIS 		AT91C_BASE_PIOB->PIO_CODR = (1<<28)

#define KIKAPCS_ON		set_outputs(13);
#define KIKAPCS_OFF		clear_outputs(13);


#define WatchdogPulse 		AT91C_BASE_PIOA->PIO_SODR = (1<<21);AT91C_BASE_PIOA->PIO_CODR = (1<<21);

#define WatchdogPulse_ON	AT91C_BASE_PIOA->PIO_SODR = (1<<21);
#define WatchdogPulse_OFF 	AT91C_BASE_PIOA->PIO_CODR = (1<<21);

#define WAKE_UP_ON 		set_outputs(7);//set_outputs(8); 
#define WAKE_UP_OFF 		clear_outputs(7);//clear_outputs(8); 


#define CS0_HI			AT91C_BASE_PIOA->PIO_SODR = (1<<30)
#define CS0_LO			AT91C_BASE_PIOA->PIO_CODR = (1<<30)

#define CS1_HI			AT91C_BASE_PIOA->PIO_SODR = (1<<28)
#define CS1_LO			AT91C_BASE_PIOA->PIO_CODR = (1<<28)

#define CS2_HI			AT91C_BASE_PIOA->PIO_SODR = (1<<27)
#define CS2_LO			AT91C_BASE_PIOA->PIO_CODR = (1<<27)

#define CS3_HI			AT91C_BASE_PIOA->PIO_SODR = (1<<4)
#define CS3_LO			AT91C_BASE_PIOA->PIO_CODR = (1<<4)

#define SI_HI			AT91C_BASE_PIOA->PIO_SODR = (1<<2)
#define SI_LO			AT91C_BASE_PIOA->PIO_CODR = (1<<2)

#define MAX_SCK_HI		AT91C_BASE_PIOA->PIO_SODR = (1<<3)
#define MAX_SCK_LO		AT91C_BASE_PIOA->PIO_CODR = (1<<3)

#define MAX_SO			(AT91C_BASE_PIOA->PIO_PDSR & (1<<29))

//DS3234

#define RTC_RST_HI	AT91C_BASE_PIOA->PIO_SODR = (1<<15)
#define RTC_RST_LO	AT91C_BASE_PIOA->PIO_CODR = (1<<15)

#define RTC_DS_HI	AT91C_BASE_PIOA->PIO_SODR = (1<<11)
#define RTC_DS_LO	AT91C_BASE_PIOA->PIO_CODR = (1<<11)

#define RTC_DIN_HI	AT91C_BASE_PIOA->PIO_SODR = (1<<14)
#define RTC_DIN_LO	AT91C_BASE_PIOA->PIO_CODR = (1<<14)

#define RTC_CLK_HI	AT91C_BASE_PIOA->PIO_SODR = (1<<13)
#define RTC_CLK_LO	AT91C_BASE_PIOA->PIO_CODR = (1<<13)

#define RTC_DOUT	(AT91C_BASE_PIOA->PIO_PDSR & (1<<12))



//

//Shift
#define i_h595_reset_hi			AT91C_BASE_PIOB->PIO_SODR = (1<<1)
#define i_h595_reset_lo			AT91C_BASE_PIOB->PIO_CODR = (1<<1)
#define i_h595_data_hi			AT91C_BASE_PIOB->PIO_SODR = (1<<7)
#define i_h595_data_lo			AT91C_BASE_PIOB->PIO_CODR = (1<<7)
#define i_h595_sh_clk_hi		AT91C_BASE_PIOB->PIO_SODR = (1<<0)
#define i_h595_sh_clk_lo		AT91C_BASE_PIOB->PIO_CODR = (1<<0)
#define i_h595_st_clk_hi		AT91C_BASE_PIOB->PIO_SODR = (1<<12)
#define i_h595_st_clk_lo		AT91C_BASE_PIOB->PIO_CODR = (1<<12)



//B�v�t�
#define e_h595_reset_hi			AT91C_BASE_PIOA->PIO_SODR = (1<<24)
#define e_h595_reset_lo			AT91C_BASE_PIOA->PIO_CODR = (1<<24)
#define e_h595_data_hi			AT91C_BASE_PIOA->PIO_SODR = (1<<25)
#define e_h595_data_lo			AT91C_BASE_PIOA->PIO_CODR = (1<<25)
#define e_h595_sh_clk_hi		AT91C_BASE_PIOA->PIO_SODR = (1<<23)
#define e_h595_sh_clk_lo		AT91C_BASE_PIOA->PIO_CODR = (1<<23)
#define e_h595_st_clk_hi		AT91C_BASE_PIOB->PIO_SODR = (1<<4)
#define e_h595_st_clk_lo		AT91C_BASE_PIOB->PIO_CODR = (1<<4)

#define e_h597_reset_hi			AT91C_BASE_PIOB->PIO_SODR = (1<<18)
#define e_h597_reset_lo			AT91C_BASE_PIOB->PIO_CODR = (1<<18)
#define e_h597_pl_hi			AT91C_BASE_PIOB->PIO_SODR = (1<<23)
#define e_h597_pl_lo			AT91C_BASE_PIOB->PIO_CODR = (1<<23)
#define e_h597_st_clk_hi		AT91C_BASE_PIOB->PIO_SODR = (1<<24)
#define e_h597_st_clk_lo		AT91C_BASE_PIOB->PIO_CODR = (1<<24)
#define e_h597_sh_clk_hi		AT91C_BASE_PIOA->PIO_SODR = (1<<26)
#define e_h597_sh_clk_lo		AT91C_BASE_PIOA->PIO_CODR = (1<<26)

#define e_h597_data			!(AT91C_BASE_PIOB->PIO_PDSR & (1<<25))

#define STOPGOMB			!(AT91C_BASE_PIOB->PIO_PDSR & (1<<16))

#define P0_ON			AT91C_BASE_PIOB->PIO_SODR = (1<<22);setOutputPin(38,3); //Perny�z�
#define P0_OFF			AT91C_BASE_PIOB->PIO_CODR = (1<<22);clearOutputPin(38,3);

#define P1_ON			AT91C_BASE_PIOB->PIO_SODR = (1<<20);setOutputPin(38,2); //P�ck�l�
#define P1_OFF			AT91C_BASE_PIOB->PIO_CODR = (1<<20);clearOutputPin(38,2);

#define P2_ON			AT91C_BASE_PIOB->PIO_SODR = (1<<21)
#define P2_OFF			AT91C_BASE_PIOB->PIO_CODR = (1<<21)


#define TARTALY_URES		(AT91C_BASE_PIOB->PIO_PDSR & (1<<5))
#define FELEGES			(AT91C_BASE_PIOB->PIO_PDSR & (1<<6))
#define VESZTERMOSZTAT		(AT91C_BASE_PIOB->PIO_PDSR & (1<<13))
#define TARTALY_TELE		!(AT91C_BASE_PIOB->PIO_PDSR & (1<<14))
#define FELEGES_FELSO		(AT91C_BASE_PIOB->PIO_PDSR & (1<<8))
#define INPUT41			!(AT91C_BASE_PIOB->PIO_PDSR & (1<<9))
#define KULSO_SZINT		!(AT91C_BASE_PIOA->PIO_PDSR & (1<<18))
#define ALSO_SZINT		!(AT91C_BASE_PIOA->PIO_PDSR & (1<<17))
#define FELSO_SZINT		!(AT91C_BASE_PIOA->PIO_PDSR & (1<<10))
#define INPUT35			!(AT91C_BASE_PIOA->PIO_PDSR & (1<<9))

#define ARAMSZUNET		(AT91C_BASE_PIOA->PIO_PDSR & (1<<22))

#define SZOBATERMOSZTAT		!(AT91C_BASE_PIOB->PIO_PDSR & (1<<15))
#define VENTI_MV		!(AT91C_BASE_PIOB->PIO_PDSR & (1<<17))

#define KULSOKEZI		!(AT91C_BASE_PIOB->PIO_PDSR & (1<<16))

#endif
