#include "AT91SAM7X256.H"
#include "Tolto.h"
#include "csiga.h"
#include "kazan.h"
#include "Board.h"
#include "shift.h"
#include "Ajto.h"
#include "data.h"
#include "CAN_data.h"
#include "expander.h"
#include "Pins.h"

volatile int terito_state,tolt1_state,tolt2_state;

volatile csiga_ teritoCS,toltoCS1,toltoCS2;

volatile csiga_ * csiga1p,* csiga2p,* csiga3p;

volatile int Toltes, toltogomb_state;
volatile int Nyitas_state, nyitgomb_state;

volatile int toltes_megy;

volatile int Toltoajto_state,idotullep_error;

volatile int idotullepes_timer,imitido_timer;
volatile int idotullepes_start,imitido_start;
volatile int imitActIdo_tmp;

volatile int ajto_wait_timer, terito_leallhat;

volatile int terito_utanfutas,terito_utanfutas_start,terito_utanfutas_timer;
volatile int toltsebTimer;
volatile int tolto_fatal_error;

volatile int razo_megy, razo_start, razo_timer;
volatile int ajto_init;

volatile int kulsoTaroloTilt;

void toltoInit(void)
{
//Ter�t�csiga
	teritoCS.forgido_mul = 1;	   					//Forgid� szorz�
	teritoCS.Elore = _teritoElore;	
	teritoCS.Hatra = _teritoHatra;
	teritoCS.Allj = _teritoAllj;
	teritoCS.Motorvedo = _teritoMV;
	teritoCS.Forgasfigyelo = _teritoFF;
//	teritoCS.get_felfutas = getTeritoFelfutas;
	teritoCS.get_forgido = getTeritoForgido;
	teritoCS.get_eloregomb = getTeritoEloreGomb;
	teritoCS.get_hatragomb = getTeritoHatraGomb;

	csiga1p = &teritoCS;

//T�lt�csiga 1
	toltoCS1.forgido_mul = 1;	   					//Forgid� szorz�
	toltoCS1.Elore = _tolt1Elore;
	toltoCS1.Hatra = _tolt1Hatra;
	toltoCS1.Allj = _tolt1Allj;
	toltoCS1.Motorvedo = _tolt1MV;
	toltoCS1.Forgasfigyelo = _tolt1FF;
//	toltoCS1.get_felfutas = getTolto1Felfutas;
	toltoCS1.get_forgido = getTolto1Forgido;
	toltoCS1.get_eloregomb = getTolto1EloreGomb;
	toltoCS1.get_hatragomb = getTolto1HatraGomb;

	csiga2p = &toltoCS1;
	
	//T�lt�csiga 2
	toltoCS2.forgido_mul = 1;	   					//Forgid� szorz�
	toltoCS2.Elore = _tolt2Elore;	
	toltoCS2.Hatra = _tolt2Hatra;
	toltoCS2.Allj = _tolt2Allj;
	toltoCS2.Motorvedo = _tolt2MV;
	toltoCS2.Forgasfigyelo = _tolt2FF;
//	toltoCS2.get_felfutas = getTolto2Felfutas;
	toltoCS2.get_forgido = getTolto2Forgido;
	toltoCS2.get_eloregomb = getTolto2EloreGomb;
	toltoCS2.get_hatragomb = getTolto2HatraGomb;

	csiga3p = &toltoCS2;

}

void toltoTenthTimer(void)
{
	csigaTimer(csiga1p);
	csigaTimer(csiga2p);
	csigaTimer(csiga3p);
	ajtoTimer();	
}

void toltoSecTimer(void)
{
	idotullepes_timer++;
	terito_utanfutas_timer++;
	toltsebTimer++;
	razo_timer++;
	ajto_wait_timer++;
	//imitido_timer++;
}

void toltoMinTimer(void)
{
	imitido_timer++;
}

void setImitidoTimer(int i)
{
 	imitido_timer = i;
}

int getImitidoTimer(void)
{
 	return imitido_timer;
}

void Tolto(void)
{	
	if(!ajto_init)
	{
	 	if(getProbalkozasSzam() > 0)			//Ha m�r van adatblokk
		{	
		 	ajto_init = 1;
			ajtoCsukas();				//Ajt� alaphelyzetbe �ll�t�sa
		}	
	}

	terito_state = csiga(csiga1p);

	if(getToltoInstalled())					//Ha van t�lt�csiga
	{
		tolt1_state = csiga(csiga2p);
	}
	
	if(getToltocsiga2Installed())				//Ha van t�lt�csiga2
	{
		tolt2_state = csiga(csiga3p);
	}

	if(getClearGomb())					//Hibat�rl�s
	{
 		clearIdotullepesHiba();
		tolto_fatal_error = 0;
	}//Hibat�rl�s

	if(tolto_fatal_error || TARTALY_TELE || getToltorendszerTiltas())
	{
	 	imitido_timer = 0;
		idotullepes_timer = 0;
	}
	Toltoajto_state = Ajto();

#if (0)
	if(!KULSO_SZINT && getToltocsiga2Installed()){setKulsoTartalyUresError();}
	else{clearKulsoTartalyUresError();}
	
	if(!KULSO_SZINT && getToltocsiga2Installed() && !getUresKulsoEseten()){kulsoTaroloTilt = 1;}
	else{kulsoTaroloTilt = 0;}
#endif


//K�zi
	if(getKezi())
	{
		Toltes = 0;
		toltes_megy = 0;
		terito_utanfutas = 0;
		terito_utanfutas_start = 0;

		if(getEkletraMehetGomb()){setCANEkletraMehet();}	//�kl�tra mehet
		else{clearCANEkletraMehet();}

		if(!getToltoInstalled())
		{
		 	if(getTolto1EloreGomb()){KULSO_TOLTO_ON;}
			else{KULSO_TOLTO_OFF;}
		}

		if(getRazomotorGomb()){RAZOMOTOR_ON;}else{RAZOMOTOR_OFF;}
	}
//Automata
	if(!getKezi())
	{
	 	if(!Toltes)
		{
		 	if(getTolto1EloreGomb() && !toltogomb_state && (Toltoajto_state == AJTO_OK)&& !TARTALY_TELE && !getIdotullepesHiba()
			&& !getAjtoNemnyitottError() && !getToltorendszerTiltas() && !tolto_fatal_error)
			{
				toltogomb_state = 1;
				teritoCS.mehet = 1;
				terito_leallhat = 0;
				ajtoNyitas();
				toltes_megy = 1;
			}
			toltsebTimer = 0;
		}
		
		if(Toltes)
		{
		 	if(getTolto1EloreGomb() && !toltogomb_state)
			{
			 	toltogomb_state = 1;
				Toltes = 0;
				terito_utanfutas = 1;
				toltes_megy = 0;
			}
		}
		
		if(getAjtoNyitva() && (Toltoajto_state == AJTO_OK) && (!TARTALY_TELE)) {Toltes = 1;}
			else
			{
				if(Toltes)
				{
			 		Toltes = 0;
					terito_utanfutas = 1;
					toltes_megy = 0;
				}
			}

		toltogomb_state = 0;

		if((TARTALY_URES || (imitido_timer >= (getImitaltToltes()*30))) && !TARTALY_TELE && (Toltoajto_state == AJTO_OK) &&
		 !getIdotullepesHiba() && !tolto_fatal_error && !getAjtoNemnyitottError() && !getAjtoNemzartError() && 
		 !getToltorendszerTiltas() && !getAkkuHianyzik())
		{
			teritoCS.mehet = 1;
			terito_leallhat = 0;
		 	ajtoNyitas();
			toltes_megy = 1;
		}
		if(getAjtoNyitva() && (Toltoajto_state == AJTO_OK) && (!TARTALY_TELE) && !getIdotullepesHiba() &&
		 !getToltorendszerTiltas()) {Toltes = 1;}
		else
		{
			if(Toltes)
			{
			 	Toltes = 0;
				toltes_megy = 0;
				terito_utanfutas = 1;
			}
		}

	}//Automata

	if(!getTolto1EloreGomb()){toltogomb_state = 0;}

	//if(TARTALY_TELE){imitido_timer = 0;}

	if(Toltes)
	{
		imitido_timer = 0;
		terito_utanfutas_timer = 0;

	 	//teritoCS.mehet = 1;				//Ha a ter�t�csiga OK, t�lt�csiga is mehet
		
		if(!getToltocsiga2Installed())			//Ha nincs t�lt�csiga 2
		{
				if((terito_state == CSIGA_OK) && getAjtoNyitva())
				{
					if(toltsebTimer >= getToltesSzunet())
					{
						toltoCS1.mehet = 1;
						KULSO_TOLTO_ON;
					}

					if(toltsebTimer > (getToltesSzunet() + getToltesBe() + 1))
					{
						if(getToltesSzunet() > 0)
						{
							toltoCS1.mehet = 0;
							KULSO_TOLTO_OFF;
						}
						toltsebTimer = 0;
					}
				}
				else
				{
					toltoCS1.mehet = 0;
					KULSO_TOLTO_OFF;
					toltsebTimer = 0;
				}
			}//Nincs t�lt� 2
			
			
			if(getToltocsiga2Installed() && KULSO_SZINT)			//Ha van t�lt�csiga 2
			{
				if(terito_state == CSIGA_OK)
				{
					toltoCS1.mehet = 1;
				}
				else {toltoCS1.mehet = 0;}
				
				
				if(tolt1_state == CSIGA_OK)
				{
					if(toltsebTimer >= getToltesSzunet())
					{
						toltoCS2.mehet = 1;
					}

					if(toltsebTimer > (getToltesSzunet() + getToltesBe() + 1))
					{
						if(getToltesSzunet() > 0)
						{
							toltoCS2.mehet = 0;
						}
						toltsebTimer = 0;
					}
				}
				else
				{
					toltoCS2.mehet = 0;
					toltsebTimer = 0;
				}
				
			}//Van t�lt� 2
			
			
		if(tolto_fatal_error)						//Fatal error, t�lt�s �llj
		{
			Toltes = 0;
			toltes_megy = 0;
			terito_utanfutas = 1;
		}

		if(!getAjtoNemzartError() && (idotullepes_timer > (getIdotullepes()*60)) && (getIdotullepes()>0))	//Ha letelt az id�
		{
		 	setIdotullepesHiba();
			terito_leallhat = 1;
			ajtoCsukas();
			Toltes = 0;
			toltes_megy = 0;
			terito_utanfutas = 1;
		}
	}//Toltes

	if(!AJTO_CSUKVA)
	{
	 	if(idotullepes_start == 0)		//Id�t�ll�p�s  start
		{
		 	idotullepes_start = 1; 
			idotullepes_timer = 0;
		}
	}

	if(!Toltes)
	//if(AJTO_CSUKVA)
	{
		idotullepes_start = 0;
	}


	if((!Toltes && !getKezi()) || !BOVITO_LEKAPCS)				//T�lt� �llj
	{
		toltoCS1.mehet = 0;
		toltoCS2.mehet = 0;
		KULSO_TOLTO_OFF;
	}

	if(((!Toltes && !getKezi() && !terito_utanfutas)) || !BOVITO_LEKAPCS)		//Ter�t� �llj
	{
	 	//teritoCS.mehet = 0;
		//ajtoCsukas();
	}

	if(!BOVITO_LEKAPCS){ajtoCsukas();}

	if(terito_utanfutas)
	{
	 	if(terito_utanfutas_start == 0)
		{
		 	terito_utanfutas_start = 1;
			terito_utanfutas_timer = 0;
		}

		if(terito_utanfutas_timer > 10)
		{	
		 	terito_utanfutas = 0;
			terito_utanfutas_start = 0;
			//teritoCS.mehet = 0;
			terito_leallhat = 1;
			toltogomb_state = 0;
			ajtoCsukas();
			toltes_megy = 0;
		}
	}

	if(terito_leallhat && getAjtoCsukva()){teritoCS.mehet = 0;}
	if(tolto_fatal_error){teritoCS.mehet = 0;}
	
	if(terito_utanfutas && BOVITO_LEKAPCS) 							//Ter�t�csiga ut�nfut�s
	{
		teritoCS.mehet = 1;
	}

	if(getAjtoNemzartError())
	{
			teritoCS.mehet = 0;
	}
	/*if(!getKezi() && !getAjtoNyitva())
	{
		terito_utanfutas = 0;
		terito_utanfutas_start = 0;
		teritoCS.mehet = 0;
	}*/

	if(!terito_utanfutas && !Toltes && !getAjtoCsukva() && TARTALY_TELE  && (getIdotullepes()>0) || \
		getIdotullepesHiba() || getTeritoElakadtError() || getTolt1ElakadtError() || getToltorendszerTiltas())///////gggggg
	{	
							  		//Ajt� becsuk�sa
	 	ajtoCsukas();
		terito_leallhat = 1;
		//clearCANEkletraMehet();
	}

	
	
	//R�z�motor
	if(Toltes)
	{
	 	if(!razo_megy)
		{
		 	if(!razo_start)
			{
				razo_start = 1;
				razo_timer = 0;
			}
		}

		if(razo_timer > getRazomotorSzunet())
		{
		 	RAZOMOTOR_ON;
			razo_megy = 1;
			razo_start = 0;
		}
	}

	if(razo_megy)
	{
	 	if(!razo_start)
		{
			 razo_start = 1;
			razo_timer = 0;
		}
		if(razo_timer > getRazomotorBe())
		{
		 	RAZOMOTOR_OFF;
			razo_megy = 0;
			razo_start = 0;
		}
	}
	
	 
	if(!getKezi())
	{
		if(Toltes){setCANEkletraMehet();setCAN70EkletraMehet();}		  				//T�lt�skor �kl�tra mehet
		else{clearCANEkletraMehet();clearCAN70EkletraMehet();}

		if((toltoCS1.mehet == 1) && (tolt1_state == CSIGA_OK)){setCAN70GyujtocsigaMehet();}
		else{clearCAN70GyujtocsigaMehet();}
	}	
	
	//Imit�lt t�lt�sig h�tral�v� id�
	if(getIdotullepesHiba()){imitido_timer = 0;}
	if((getImitaltToltes()*30) < imitido_timer)
	{
		imitActIdo_tmp = 0;
	}
	else
	{
		imitActIdo_tmp = (getImitaltToltes()*30) - imitido_timer;
	}
	
	if(Toltes || tolto_fatal_error){imitActIdo_tmp = 0;}
	if(tolto_fatal_error){imitActIdo_tmp = 65000;}
	setImitActIdo(imitActIdo_tmp);


	if(TARTALY_URES && TARTALY_TELE){setSzintjelzoHiba();}
	else{clearSzintjelzoHiba();}

	if(getIdotullepesHiba()){tolto_fatal_error = 1;}
	if(getAjtoNyitvakapcsoloRossz()){tolto_fatal_error = 1;}
	if(getAjtoZarvakapcsoloRossz()){tolto_fatal_error = 1;}
	if(getAjtoNemnyitottError()){tolto_fatal_error = 1;}
	if(getAjtoNemzartError()){tolto_fatal_error = 1;setSMSajtonyitva();setCANToltoajtoNemzart();}
	else{clearSMSajtonyitva();clearCANToltoajtoNemzart();}

	if(toltes_megy){setToltesmegy();}else{clearToltesmegy();}

	//Hib�k elk�ld�se
	if(getToltoInstalled())
	{
	  if(tolt1_state == CSIGA_PROBALKOZIK){setTolt1ProbalError();}else{clearTolt1ProbalError();}
		if(tolt1_state == CSIGA_ELAKADT)
		{
			setTolt1ElakadtError();tolto_fatal_error = 1;
			clearCANEkletraMehet();clearCAN70EkletraMehet();
		}else{clearTolt1ElakadtError();}
		//if(!TOLTO1_MOTORVEDO && BOVITO_LEKAPCS){setTolt1MVError();}else{clearTolt1MVError();}
		if(tolt1_state == MV_NEMKAPCSOLT_VISSZA){setTolt1MVnemkapcsoltError();}else{clearTolt1MVnemkapcsoltError();}
	}

	if(getToltocsiga2Installed())
	{
		if(tolt2_state == CSIGA_PROBALKOZIK){setTolt2ProbalError();}else{clearTolt2ProbalError();}
		if(tolt2_state == CSIGA_ELAKADT)
		{
			setTolt2ElakadtError();tolto_fatal_error = 1;
			clearCANEkletraMehet();clearCAN70EkletraMehet();
		}else{clearTolt2ElakadtError();}
		//if(!TOLTO2_MOTORVEDO && BOVITO_LEKAPCS){setTolt2MVError();}else{clearTolt2MVError();}
		if(tolt2_state == MV_NEMKAPCSOLT_VISSZA){setTolt2MVnemkapcsoltError();}else{clearTolt2MVnemkapcsoltError();}
	}

	if(terito_state == CSIGA_PROBALKOZIK){setTeritoProbalError();}else{clearTeritoProbalError();}
	if(terito_state == CSIGA_ELAKADT){setTeritoElakadtError();tolto_fatal_error = 1;}else{clearTeritoElakadtError();}
	//if(!TERITO_MOTORVEDO && BOVITO_LEKAPCS){setTeritoMVError();}else{clearTeritoMVError();}
	if(terito_state == MV_NEMKAPCSOLT_VISSZA){setTeritoMVnemkapcsoltError();}else{clearTeritoMVnemkapcsoltError();}
	
	if((terito_state == CSIGA_ELAKADT) || (terito_state == MV_LEKAPCSOLT) || (terito_state == MV_NEMKAPCSOLT_VISSZA) ||
	(tolt1_state == CSIGA_ELAKADT) || (tolt1_state == MV_LEKAPCSOLT) || (tolt1_state == MV_NEMKAPCSOLT_VISSZA) || 
	getAjtoNemnyitottError() || getAjtoNemzartError() || getAjtoNyitvakapcsoloRossz() || getAjtoZarvakapcsoloRossz() || getIdotullepesHiba())
	{setCANToltoHiba();clearCANEkletraMehet();clearCAN70EkletraMehet();}else{clearCANToltoHiba();}

	if(tolto_fatal_error == 1){setSMStoltohiba();}
	else{clearSMStoltohiba();}

}//Tolto
