#include "AT91SAM7X256.H" 
#include "FM25CL64B.h"               
#include "Board.h"

#define WRITE	2
#define READ	3

void waitFM(int i)
{
 	i*=10;
	while(i>0){i--;}
}

void writeSPI(int size, int data)
{
	size*=8;
	
	while(size > 0)
	{
		if((data & (1<<(size-1))) > 0){SI_HI;}
		else {SI_LO;}
		waitFM(1);
		MAX_SCK_HI;
		waitFM(1);
		MAX_SCK_LO;
		waitFM(1);
		size--;
	} 
	SI_LO;
	waitFM(5);	
}

int readSPI()
{			
	int i, data;
	waitFM(1);
	
	for(i=8;i>0;i--)
	{
		MAX_SCK_HI;waitFM(1);
	 	if(MAX_SO > 0){data |= (1 << (i-1));}
		else {data &= ~(1 << (i-1));}
		MAX_SCK_LO;waitFM(1);	
	}
	return data;	
}


void writeFM25CL64B(int address, int size, unsigned char data[])
{
	int x = 0;
	MAX_SCK_LO;
	CS3_LO;
	waitFM(1);
	writeSPI(1,6);
	waitFM(1);
	CS3_HI;
	waitFM(1);
	CS3_LO;


	writeSPI(1,WRITE);
	writeSPI(2,address);

	while(x < size)
	{
		writeSPI(1,data[x]);
	 	x++;
	}

	waitFM(10);
	CS3_HI;
	waitFM(10);
}


void readFM25CL64B(int address, int size, unsigned char data[])
{
	int x = 0;
	MAX_SCK_LO;
	CS3_LO;
	waitFM(1);

	writeSPI(1,READ);
	writeSPI(2,address);

	while(x < size)
	{
		data[x] = readSPI();
	 	x++;
	}

	waitFM(10);
	CS3_HI;
}


