#include <stdio.h>                          
#include "AT91SAM7X256.H" 


#define MCK             ((18432000*73/14)/2)   // Output PLL Clock

#if (0)

//Init
__attribute__ ((section (".ramfunc"))) void flashInit(void) 
{
	AT91C_BASE_MC->MC_FMR = ((AT91C_MC_FMCN) & (72 << 16)) | AT91C_MC_FWS_1FWS ;       // Number Of MCK Cylcles in 1.5 us; 1.5 * 48 = 72
	//AT91C_BASE_MC->MC_FMR = AT91C_MC_FWS_1FWS | (1 + (((MCK * 15) / 10000000)) << 16);
	//AT91C_BASE_MC->MC_FMR = AT91C_MC_FWS_1FWS | (1 + (((MCK * 15) / 10000000)) << 16);
}

//Write
__attribute__ ((section (".ramfunc"))) unsigned int flashWrite(void) 
{

   unsigned int flashaddr = 0x0013FF00;
   
   unsigned int * wrptr;   
   unsigned int status;
   
   wrptr =  (unsigned int *)flashaddr;                                                
   *wrptr = 0x12345678;

   
   //AT91C_BASE_MC->MC_FMR &= ~AT91C_MC_NEBP;                               // No Erase Before Programming

   while(!(AT91C_BASE_MC->MC_FSR & AT91C_MC_FRDY));                       // check if FRDY flag is set
      
   AT91C_BASE_MC->MC_FCR = ((0x5A << 24) & AT91C_MC_KEY) | ((1023 << 8) & AT91C_MC_PAGEN) | AT91C_MC_FCMD_START_PROG;    
   
   //status = AT91C_BASE_MC->MC_FSR;
   
/*   if(status & (0x1 << 2)) 
      putc("Lock Error\n");
      
   if(status & (0x1 << 3)) 
      putc("Programming Error\n"); */
   
   while(!(AT91C_BASE_MC->MC_FSR & AT91C_MC_FRDY));

   status = AT91C_BASE_MC->MC_FSR;

   return status;
} 


//Read
__attribute__ ((section (".ramfunc"))) void flashRead(void) 
{
//   unsigned long *flashaddr = 0x0013FF00;
   //nprintf(*flashaddr); putchar('\n');  
}

#endif
