#ifndef __KAZAN_H
#define __KAZAN_H

// #define KAZAN_TEST	1

void pwm0_on(void);
void pwm0_off(void);
int getAramszunet(void);


void kazan(void);
void kazanTenthTimer(void);
void kazanSecTimer(void);
void kazanMinTimer(void);

#endif
