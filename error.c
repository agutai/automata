#include "AT91SAM7X256.H"
#include "error.h"
#include "Board.h"
#include "data.h"
#include "kazan.h"

int bell_counter, _isError;

int isError(void)
{
 	return _isError;
}


void error(void)
{
//Cseng� kapcsolgat�sa
  
	if((TARTALY_URES && !getFauzem()) || (FELEGES && getBurninAlarm()) || VESZTERMOSZTAT || ARAMSZUNET || getKihultError() ||
	 getOverheatError() || (getTulfutes() && getTulfutesAlarm()) || getToltohiba() || getSalakolohiba() || getEkletrahiba() ||
	 getStopError() || getError35() || getError41())
	{
		_isError = 1;

	        switch (bell_counter)
	        {
	            case 1:     BELL_ON; ERROR_LED_ON;break;	
	            case 2:     ERROR_LED_OFF;if(!getCsengokimenetFolyamatos()){BELL_OFF;}break;
	            case 3:     ERROR_LED_ON;break;
	            case 4:     ERROR_LED_OFF;break;
	            case 5:     ERROR_LED_ON;break;
	            case 6:     ERROR_LED_OFF;break;
	            case 7:     ERROR_LED_ON;break;
	            case 8:     ERROR_LED_OFF;break;
	            case 9:     ERROR_LED_ON;break;
	            case 10:    ERROR_LED_OFF;bell_counter = 0; break;              
	        }
	        bell_counter++; 
	}
	else
	{
		_isError = 0;
		BELL_OFF; ERROR_LED_OFF;
		bell_counter = 0; 
	}
}
