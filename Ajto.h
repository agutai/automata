#ifndef __AJTO_H
#define __AJTO_H

#define AJTO_NOT_OK				0
#define AJTO_OK					1
#define AJTO_PROBALKOZIK			2
#define AJTO_ELAKADT				3

#define AJTO_START_SZUNET_NYITAS		50
#define AJTO_START_SZUNET_ZARAS			10
#define AJTO_START_SZUNET_PROBA			30
#define AJTOKAPCSOLO_TIMEOUT			80

#define AJTO_HATRAMENET				30

int Ajto(void);
void ajtoTimer(void);

void ajtoNyitas(void);
void ajtoCsukas(void);
void ajtoAllj(void);
int getAjtoNyitva(void);
int getAjtoCsukva(void);
int getAjtoHiba(void);

#endif
