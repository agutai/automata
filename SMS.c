#include <rtl.h>
#include "Board.h"
#include "data.h"
#include "CAN_data.h"
#include "sms.h"

unsigned char sms_rx_buff[200];
unsigned char sms_tx_buff[200];

extern void sendchar0(int);

#define RS_NUM_TESTS 48
static unsigned char rs_sync_ok[16];
static unsigned char rs_sync_test = RS_NUM_TESTS;

unsigned short CRC16(unsigned char *msg, unsigned short len);

unsigned char sensor_data[160];

void clear_sensor_data()
{
	int x;
	for (x=0; x<160; x++)
		sensor_data[x] = 0;
}

void set_sensor_data(int idx, unsigned char *data)
{
	int x;
	for (x=0; x<10; x++)
		sensor_data[idx*10+x] = data[x];
}


void fill_sensor_data(unsigned char *buff)
{
	static int sensor_data_idx = 0;
	int x;

	for (x=0; x<4; x++) {
		unsigned char y;

		buff[136+x*11] = sensor_data_idx;
		for (y=0; y<10; y++)
			buff[136+x*11+y+1] = sensor_data[sensor_data_idx*10+y];

		do {
			sensor_data_idx++;
		} while ((sensor_data_idx < 16) && (rs_sync_ok[sensor_data_idx] == 0));
		if (sensor_data_idx == 16)
			sensor_data_idx = 0;
	}
}

// #define EKLETRA

int check_sms_comm(int len)
{
	int ret = 0;
	
	if((len == 199)&&(sms_rx_buff[197] == 0XFA)&&(sms_rx_buff[198] == 0XFA)&&(sms_rx_buff[199] == 0XFA))
	{
		unsigned short crc_data, crc_calc;

		crc_data = (sms_rx_buff[195] << 8) | sms_rx_buff[196];
		crc_calc = CRC16(sms_rx_buff, 195);

		if (crc_data == crc_calc) {
			set_rx_Data(sms_rx_buff,1);
#ifdef EKLETRA
#else
			if (getKazanStop()) {
				if (getKazanReset())
					clearStopError();
				else
					setStopError();
				clearKazanStop();
			}
#endif
			ret = 1;
		}
	}

	if((len == 17)&&(sms_rx_buff[0] == 'B')&&(sms_rx_buff[1] == 'U')&&(sms_rx_buff[2] == 'S')&&(sms_rx_buff[3] == ':'))
	{
		unsigned char addr = 0;
		unsigned short crc,crc_rx;

		addr = sms_rx_buff[4];
		crc_rx = (sms_rx_buff[15] << 8) + sms_rx_buff[16];
		crc = CRC16(sms_rx_buff,15);
		if ((addr < 16) && (crc_rx == crc)) {
			set_sensor_data(addr, sms_rx_buff+5);
			if (rs_sync_test) rs_sync_ok[addr]++;
		}
	}

	return ret;
}

void sendSMSmsg(void)
{
	static unsigned char sms_tx_toggle = 0;
	static unsigned char rs_address = 0;
	unsigned short crc;
	int x;

	if (rs_sync_test == RS_NUM_TESTS) {
		for (x=0; x<16; x++) {
			rs_sync_ok[x] = 0;
		}
	}

	if ((sms_tx_toggle & 1) || (rs_sync_test)) { // kulso eszkoz
		RS485_ENA;
		for (x=0; x<15; x++)
			sms_tx_buff[x] = 0;
		sms_tx_buff[0] = 'R';
		sms_tx_buff[1] = 'E';
		sms_tx_buff[2] = 'L';
		sms_tx_buff[3] = ':';
		sms_tx_buff[4] = rs_address;
		if (rs_address == 16) {
#ifdef EKLETRA
#else
			sms_tx_buff[5] = (getKazanNum() << 4) + (getKazanType() & 0x0f);
			sms_tx_buff[6] = getHomero1() >> 8;
			sms_tx_buff[7] = getHomero1() & 0xff;
			sms_tx_buff[8] = getHomero2() >> 8;
			sms_tx_buff[9] = getHomero2() & 0xff;
			if ((getKazanType() == 5) ||(getKazanType() == 6) ||(getKazanType() == 8)) {
				sms_tx_buff[10] = getGozNyomasmero(); 
				sms_tx_buff[11] = getGoznyomas();
			} else {
				sms_tx_buff[10] = getHofok1() >> 8;
				sms_tx_buff[11] = getHofok1() & 0xff;
			}
			sms_tx_buff[12] = 0;
			if (getCANAramszunet()) sms_tx_buff[12] |= 0x01;
			if (getCANVesztermosztat()) sms_tx_buff[12] |= 0x02;
			if (getCANKazanKihult()) sms_tx_buff[12] |= 0x04;
			if (getCANVizszintAlacsony()) sms_tx_buff[12] |= 0x08;
			if (getCANToltoHiba()) sms_tx_buff[12] |= 0x02;
			if (getCANToltoajtoNemzart()) sms_tx_buff[12] |= 0x04;
			if (getCANSalakoloHiba()) sms_tx_buff[12] |= 0x08;

			sms_tx_buff[13] = 0;
			sms_tx_buff[14] = 0;
#endif
		} else {
			sms_tx_buff[5] = get_relay_state(rs_address);
		}
		crc = CRC16(sms_tx_buff,15);
		sms_tx_buff[15] = crc >> 8;
		sms_tx_buff[16] = crc;
		for(x=0;x<17;x++)
			sendchar0(sms_tx_buff[x]);
		os_dly_wait(2);
		RS485_DIS;

		if (rs_sync_test) {
			rs_address++;
			if (rs_address == 16) rs_address = 0;
			rs_sync_test--;
		} else {
			do {
				rs_address++;
			} while ((rs_address < 16) && (rs_sync_ok[rs_address] == 0));
			if (rs_address == 16)
				rs_address = 0;
		}

	} else { // sms modul
		RS485_DIS;
		if (sms_tx_toggle == 2)
			set_tx_Data(sms_tx_buff, 1);
		else
			set_tx_Data(sms_tx_buff, 0);
		sendchar0('A');
		sendchar0('N');
		sendchar0('D');
		sendchar0('R');
		sendchar0(':');
		for(x=0;x<200;x++)
			sendchar0(sms_tx_buff[x]);
		os_dly_wait(2);
		RS485_ENA;
	}

	sms_tx_toggle++;
	sms_tx_toggle &= 3;
} 
