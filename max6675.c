#include "AT91SAM7X256.H"
#include "max6675.h"
#include "Board.h"


int readMax6675(int i)
{
	volatile int count1;
	volatile unsigned int temp;

	switch (i)
	{
	 	case 0:
			 CS0_LO;
			 break;

		case 1:
			 CS1_LO;
			 break;

		case 2:
			 CS2_LO;
		 	 break;
	}
 	temp = 0;

	for(count1 = 0; count1 < 11;count1++)
	{
	 	MAX_SCK_HI;
		if(MAX_SO)
		{
			temp+=1;temp = temp<<1;
		}	
		else 
		{	
			temp = temp<<1;
		}
		MAX_SCK_LO;
	}

	CS0_HI;CS1_HI;CS2_HI;
	return temp>>1;
}

