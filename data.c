#include "main.h"
#include "data.h"
#include "CAN_data.h"
#include "board.h"
#include "expander.h"
#include "Tolto.h"
#include "FM25CL64B.h"
#include "Kazan.h"
#include "sms.h"

unsigned char rx_data[220];
unsigned char tx_data[220];


int getTabletBit(void)
{
	if (rx_data[127] & 0x01)
		return 1;
	else
		return 0;
}

void setTabletBit(int bit)
{
	if (bit)
		tx_data[194] |= 0x01;
	else
		tx_data[194] &= 0xfe;
}

void set_tx_Data(unsigned char i[], int init)
{
	int x;

	if(init)	//Ha Init OK
	{
		for(x=0;x<197;x++)
		{
			i[x] = tx_data[x];
		}

		fill_sensor_data(i);

		i[197] = 0XFA;
		i[198] = 0XFA;
		i[199] = 0XFA;
	}
	else
	{
		unsigned short crc_calc;

		for(x=0;x<197;x++)
		{
			i[x] = rx_data[x];
		}
		crc_calc = CRC16(i, 195);
		i[195] = crc_calc >> 8;
		i[196] = crc_calc & 0xff;

		i[197] = 0XFB;
		i[198] = 0XFB;
		i[199] = 0XFB;
	}
}

void  set_rx_Data(unsigned char i[], int init)
{
	int x;
	
	if(init)		//Ha Init OK, adatok t�rol�sa
	{
		for(x=0;x<200;x++)
		{
			rx_data[x] = i[x];
		}
	}  
}


int readDS3234(int);

void loadData()
{
	int i;
	unsigned char c;
	
	readFM25CL64B(0,200,rx_data);
	
	readFM25CL64B(300,1,&c);
	setImitidoTimer(readDS3234(c));
	
	readFM25CL64B(301,1,&c);
	setSalakszunetTimer(readDS3234(c));
	
	readFM25CL64B(302,1,&c);
	setUzemoraTimer(c);
	

	for(i=0;i<6;i++)
	{
		rx_data[i] = 0;
	}
}

void saveData()
{
	unsigned char c;

	writeFM25CL64B(0,200,rx_data);

	c = getImitidoTimer();
	writeFM25CL64B(300,1,&c);

	c = getSalakszunetTimer();
	writeFM25CL64B(301,1,&c);

	c = getUzemoraTimer();
	writeFM25CL64B(302,1,&c);
}

void setHomero(int num,int value)
{
	switch (num)
	{
	case 0:
		tx_data[0] = value>>8;tx_data[1] = value;
		break;
	
	case 1:
		tx_data[2] = value>>8;tx_data[3] = value;
		break;
	
	case 2:
		tx_data[4] = value>>8;tx_data[5] = value;
		break;
	}
}

void setGozNyomasmero(int i)
{
	tx_data[7] = i;
	tx_data[6] = i >> 8;
}

int getGozNyomasmero(void)
{
	return ((tx_data[6]<<8)+tx_data[7]);
}

void setHidraulikaNyomasmero(unsigned char buff[])
{
	tx_data[8] = buff[0];
	tx_data[9] = buff[1];
}

int getHidraulikaNyomasmero(void)
{
	return ((tx_data[8]<<8)+tx_data[9]);
}

void setAkkufesz(unsigned char buff[])
{
	tx_data[10] = buff[0];
	tx_data[11] = buff[1];
}

int getAkkufesz(void)
{
	return ((tx_data[10]<<8)+tx_data[11]);
}

void setTuzterNyomasmero(unsigned char buff[])
{
	tx_data[12] = buff[0];
	tx_data[13] = buff[1];
}

int getTuzterNyomasmero(void)
{
	return ((tx_data[12]<<8)+tx_data[13]);
}

void setSMSaramszunet(void){tx_data[180] |= (1 << 0);}
void clearSMSaramszunet(void){tx_data[180] &= ~(1 << 0);}

void setSMSvesztermosztat(void){tx_data[180] |= (1 << 1);}
void clearSMSvesztermosztat(void){tx_data[180] &= ~(1 << 1);}

void setSMSfeleges(void){tx_data[180] |= (1 << 2);}
void clearSMSfeleges(void){tx_data[180] &= ~(1 << 2);}

void setSMSurestartaly(void){tx_data[180] |= (1 << 3);}
void clearSMSurestartaly(void){tx_data[180] &= ~(1 << 3);}

void setSMSvizszintalacsony(void){tx_data[180] |= (1 << 4);}
void clearSMSvizszintalacsony(void){tx_data[180] &= ~(1 << 4);}

void setSMStuzkialudt(void){tx_data[180] |= (1 << 5);}
void clearSMStuzkialudt(void){tx_data[180] &= ~(1 << 5);}

void setSMStulmelegedes(void){tx_data[180] |= (1 << 6);}
void clearSMStulmelegedes(void){tx_data[180] &= ~(1 << 6);}

void setSMStulfutes(void){tx_data[180] |= (1 << 7);}
void clearSMStulfutes(void){tx_data[180] &= ~(1 << 7);}

void setSMStoltohiba(void){tx_data[181] |= (1 << 0);}
void clearSMStoltohiba(void){tx_data[181] &= ~(1 << 0);} 

int getToltohiba()
{
	if((tx_data[181]&0X01) > 0)return 1;
	else return 0;
}

void setSMSajtonyitva(void){tx_data[181] |= (1 << 1);}
void clearSMSajtonyitva(void){tx_data[181] &= ~(1 << 1);}
int getSMSAjtonyitva()
{
	if((tx_data[181]&0X02) > 0)return 1;
	else return 0;
}

void setSMSsalakolohiba(void){tx_data[181] |= (1 << 2);}
void clearSMSsalakolohiba(void){tx_data[181] &= ~(1 << 2);}
int getSalakolohiba()
{
	if((tx_data[181]&0X04) > 0)return 1;
	else return 0;
}

void setSMSekletrahiba(void){tx_data[181] |= (1 << 3);}
void clearSMSekletrahiba(void){tx_data[181] &= ~(1 << 3);}
int getEkletrahiba()
{
	if((tx_data[181]&0X08) > 0)return 1;
	else return 0;
}

void setSMSstopError(void) { tx_data[181] |= (1 << 4); }
void clearSMSstopError(void) { tx_data[181] &= ~(1 << 4); }

void setSMSerror35(void) { tx_data[181] |= (1 << 5); }
void clearSMSerror35(void) { tx_data[181] &= ~(1 << 5); }

void setSMSerror41(void) { tx_data[181] |= (1 << 6); }
void clearSMSerror41(void) { tx_data[181] &= ~(1 << 6); }

void refreshData(void)
{
	if(VESZTERMOSZTAT)
	{
		tx_data[30] |= (1 << 1);
		setCANVesztermosztat();
		setSMSvesztermosztat();
	} 
	else
	{
		tx_data[30] &= ~(1 << 1);
		clearCANVesztermosztat();
		clearSMSvesztermosztat();
	} 

	if(getAramszunet())
	{
		tx_data[30] |= (1 << 0);
		setCANAramszunet();
		setSMSaramszunet();
	} 
	else
	{
		tx_data[30] &= ~(1 << 0);
		clearCANAramszunet();
		clearSMSaramszunet();
	} 
	            
	if(SZOBATERMOSZTAT){tx_data[30] |= (1 << 2);} else{tx_data[30] &= ~(1 << 2);}
	if(!VENTI_MV && BOVITO_LEKAPCS){tx_data[31] |= (1 << 2);} else{tx_data[31] &= ~(1 << 2);}

	if(TARTALY_URES)
	{
		tx_data[30] |= (1 << 3);
		if(getToltoInstalled() && getLowlevelEnable() || getFauzem())
		{
			clearSMSurestartaly();
		}
		else
		{
			setSMSurestartaly();
		}
	} 
	else
	{
		tx_data[30] &= ~(1 << 3);
		clearSMSurestartaly();
	}
	if(TARTALY_TELE){tx_data[30] |= (1 << 4);} else{tx_data[30] &= ~(1 << 4);}
	if(FELEGES)
	{
		tx_data[30] |= (1 << 5);
		if(getBurninAlarm()){setSMSfeleges();}
	} 
	else
	{
		tx_data[30] &= ~(1 << 5);
		clearSMSfeleges();
	}
	if(FELEGES_FELSO){tx_data[30] |= (1 << 6);} else{tx_data[30] &= ~(1 << 6);}
	if(KULSO_SZINT) {tx_data[30] |= (1 << 7); } else { tx_data[30] &= ~(1 << 7);}

	if(ALSO_SZINT){tx_data[31] |= (1 << 0);} else{tx_data[31] &= ~(1 << 0);}
	if(FELSO_SZINT){tx_data[31] |= (1 << 1);} else{tx_data[31] &= ~(1 << 1);}

	if(INPUT35) {
		tx_data[31] |= (1 << 3);
		if (getError35Default() > 0)
			setError35();
		else if (!getError35Stop() || getError35Restart()) clearError35();
	} else {
		tx_data[31] &= ~(1 << 3);
		if (getError35Default() == 0)
			setError35();
		else if (!getError35Stop() || getError35Restart()) clearError35();
		
	}
	if(INPUT41) {
		tx_data[31] |= (1 << 4);
		setError41();
	} else {
		tx_data[31] &= ~(1 << 4);
		clearError41();
	}

	if (getGozos() && getErrorVizszintAlacsony())
	{
		setSMSvizszintalacsony();
	}
	else
	{
		clearSMSvizszintalacsony();
	}

	if(getCAN70errorEkletraIdotullepes() || getCAN70errorCsiga1Elakadt()){setSMSekletrahiba();}
	else{clearSMSekletrahiba();}

	if(TOLTO1_FORGASFIGYELO){tx_data[32] |= (1 << 0);} else{tx_data[32] &= ~(1 << 0);}
	if(TOLTO2_FORGASFIGYELO){tx_data[32] |= (1 << 1);} else{tx_data[32] &= ~(1 << 1);}
	if(TERITO_FORGASFIGYELO){tx_data[32] |= (1 << 2);} else{tx_data[32] &= ~(1 << 2);}
	if(SALAKOLO_FORGASFIGYELO){tx_data[32] |= (1 << 3);} else{tx_data[32] &= ~(1 << 3);}
	if(FELHORDO1_FORGASFIGYELO){tx_data[32] |= (1 << 4);} else{tx_data[32] &= ~(1 << 4);}
	if(FELHORDO2_FORGASFIGYELO){tx_data[32] |= (1 << 5);} else{tx_data[32] &= ~(1 << 5);}
	if(POCKOLO_FORGASFIGYELO){tx_data[32] |= (1 << 6);} else{tx_data[32] &= ~(1 << 6);}
	if(PERNYE_FORGASFIGYELO){tx_data[32] |= (1 << 7);} else{tx_data[32] &= ~(1 << 7);}

	if(AJTO_NYITVA){tx_data[33] |= (1 << 0);} else{tx_data[33] &= ~(1 << 0);}
	if(AJTO_CSUKVA){tx_data[33] |= (1 << 1);} else{tx_data[33] &= ~(1 << 1);}
	if(VEDOBERENDEZESEK_KORE){tx_data[33] |= (1 << 2);} else{tx_data[33] &= ~(1 << 2);}
	if(KULSOKEZI){tx_data[33] |= (1 << 7);} else{tx_data[33] &= ~(1 << 7);}

	if(SALAKOLO_MOTORVEDO){tx_data[34] |= (1 << 2);} else{tx_data[34] &= ~(1 << 2);}
	if(FELHORDO1_MOTORVEDO){tx_data[34] |= (1 << 3);} else{tx_data[34] &= ~(1 << 3);}
	if(FELHORDO2_MOTORVEDO){tx_data[34] |= (1 << 4);} else{tx_data[34] &= ~(1 << 4);}
	if(TOLTO1_MOTORVEDO){tx_data[34] |= (1 << 5);} else{tx_data[34] &= ~(1 << 5);}
	if(TOLTO2_MOTORVEDO){tx_data[34] |= (1 << 6);} else{tx_data[34] &= ~(1 << 6);}
	if(TERITO_MOTORVEDO){tx_data[34] |= (1 << 7);} else{tx_data[34] &= ~(1 << 7);}

//	if(getCANEkletraErrorCsiga1Elakadt()){tx_data[50] |= (1 << 1);} else{tx_data[50] &= ~(1 << 1);}
//	if(getCANEkletraErrorElakadt()){tx_data[50] |= (1 << 7);} else{tx_data[50] &= ~(1 << 7);}
//	if(getCANEkletraErrorIdotullepes()){tx_data[51] |= (1 << 0);} else{tx_data[51] &= ~(1 << 0);}
//	if(getCANKulsoSalakoloError()){tx_data[51] |= (1 << 1);} else{tx_data[51] &= ~(1 << 1);}

//	if(getCAN70errorEkletraIdotullepes()){tx_data[48] |= (1 << 0);} else{tx_data[48] &= ~(1 << 0);}
//	if(getCAN70errorCsiga1Probal()){tx_data[48] |= (1 << 1);} else{tx_data[48] &= ~(1 << 1);}
//	if(getCAN70errorCsiga1Elakadt()){tx_data[48] |= (1 << 2);} else{tx_data[48] &= ~(1 << 2);}

	tx_data[48] = getCAN70error();		//�kl�tra hiba�zenetek
	tx_data[35] = getCAN70inputs();		//�kl�tra bemenetek
	tx_data[55] = getCAN70outputs();	//�kl�tra kimenetek

	tx_data[183] = getSec();
	tx_data[184] = getMin();
	tx_data[185] = getHour();
	tx_data[186] = getDate();
	tx_data[187] = getMonth();
	tx_data[188] = getYear();
}

void settingTime()
{
	setMin(rx_data[120]);
	setHour(rx_data[121]);
	setDate(rx_data[122]);
	setMonth(rx_data[123]);
	setYear(rx_data[124]);
}


void setVersion(int x, int y)
{
	tx_data[195] = x;
	tx_data[196] = y;
}


//Kimenetek �llapotnak jelz�se a kijelz� fel�
void setOutputPin(int block,int pin)
{
	tx_data[block] |= (1 << pin);
}

void clearOutputPin(int block,int pin)
{
	tx_data[block] &= ~(1 << pin);
}

int getHomero1(void)
{
	return ((tx_data[0]<<8)+tx_data[1]);
}

int getHomero2(void)
{
	return ((tx_data[2]<<8)+tx_data[3]);
}

int getHomero3(void)
{
	return ((tx_data[4]<<8)+tx_data[5]);
}

//Bolygat� �ramm�r�
void setBolygatoArammero(int i)
{
	tx_data[15] = i;
}

int getBolygatoArammero()
{
	return tx_data[15];
}

//T�lt�ajt� �ramm�r�
void setToltoajtoArammero(int i)
{
	tx_data[16] = i;
}

int getToltoajtoArammero(void)
{
	return tx_data[16];
}

//Salakajt� �ramm�r�
void setSalakajtoArammero(int i)
{
	tx_data[17] = i;
}

int getSalakajtoArammero(void)
{
	return tx_data[17];
}

//Pernyeajt� �ramm�r�
void setPernyeajtoArammero(int i)
{
	tx_data[18] = i;
}

int getPernyeajtoArammero(void)
{
	return tx_data[18];
}


//----------------------------------------------------------------------------------------


int getHofok1(void)
{
	return ((rx_data[9]<<8)+rx_data[8]);	//Felcser�lve
}

int getVizhoHyst(void)
{
	return rx_data[10];
}

int getRostelyseb(void)
{
	return rx_data[12];
}

int getAlloBolyg(void)
{
	return rx_data[14];
}

int getUzemiBolyg(void)
{
	return rx_data[15];
}

int getBolygBe(void)
{
	return rx_data[16];
}

void setBolygBe(int i)
{
	rx_data[16] = i;
}

int getBolygatoAramkorlat(void)
{
	return rx_data[17];
}

int getTulfutHyst(void)
{
	return rx_data[18];
}

int getKihulHyst(void)
{
	return rx_data[19];
}

int getKihulIdo(void)
{
	return rx_data[20];
}

int getTimerBe(void)
{
	return rx_data[21];
}

int getTimerSzunet(void)
{
	return rx_data[22];
}

int getFigyelmeztetesiHofok(void)
{
	return rx_data[23];
}

int getKazanNum(void)
{
	return rx_data[24];
}

int getTuzoltasTime(void)
{
	return rx_data[25];
}

int getGoznyomas(void)
{
	return rx_data[26];
}

int getFelsoNyomasHyst(void)
{
	return rx_data[27];
}

int getAlsoNyomasTav(void)
{
	return rx_data[28];
}

int getVizhoMax(void)
{
	return ((rx_data[29]<<8)+rx_data[30]);
}

int getGoznyomasMax(void)
{
	return rx_data[31];
}


int getRazomotorSzunet(void)
{
	return rx_data[32];
}

int getRazomotorBe(void)
{
	return rx_data[33];
}

int getHofok3(void)
{
	return ((rx_data[34]<<8)+rx_data[35]);
}

int getVizhoKomp(void)
{
	return rx_data[36];
}

int getGoznyomasKomp(void)
{
	return rx_data[37];
}

int getKazanType(void)
{
	return rx_data[38];
}





//02
int getImitaltToltes(void)
{
	return rx_data[48];
}

int getIdotullepes(void)
{
	return rx_data[49];
}

int getProbalkozasSzam(void)
{
	return rx_data[50];
}

int getToltoajtoAramkorlat(void)
{
	return rx_data[51];
}

int getPernyecsigaAramkorlat(void)
{
	return rx_data[52];
}

int getPockoloAramkorlat(void)
{
	return rx_data[53];
}

int getPernyezoForgido(void)
{
	return rx_data[54];
}

int getPockoloForgido(void)
{
	return rx_data[55];
}

int getSalakoloForgido(void)
{
	return rx_data[56];
}

int getTolto1Forgido(void)
{
	return rx_data[57];
}

int getTolto2Forgido(void)
{
	return rx_data[58];
}

int getFelhordo1Forgido(void)
{
	return rx_data[59];
}

int getFelhordo2Forgido(void)
{
	return rx_data[60];
}

int getTeritoForgido(void)
{
	return rx_data[61];
}

int getSalakBe(void)
{
	return rx_data[62];
}

int getSalakSzunet(void)
{
	return rx_data[63];
}

int getFelhordoUtanfutas(void)
{
	return rx_data[64];
}

int getPernyeFelfutas(void)
{
	return rx_data[64];
}

int getToltesSzunet(void)
{
	return rx_data[65];
}

int getToltesBe(void)
{
	return rx_data[66];
}


//�kl�tra ID70 adatok

int getEkletraIdotullepes(void)
{
	return rx_data[70];
}

int getEkletraElakadasinyomas(void)
{
	return rx_data[71];
}

int getEkletraMaxnyomas(void)
{
	return rx_data[72];
}

int getGyujto1Forgido(void)
{
	return rx_data[73];
}

int getGyujto2Forgido(void)
{
	return rx_data[74];
}
//CAN ID70




//H�tral�v� salakol�si id�
void setSalakActIdo(int i)
{
	tx_data[20] = i;
	tx_data[19] = (i>>8);
}
//H�tral�v� salakol�s sz�netid�
void setSalakszunetActIdo(int i)
{
	tx_data[22] = i;
	tx_data[21] = (i>>8);
}
//H�tral�v� id� imit�lt t�lt�sig
void setImitActIdo(int i)
{
	tx_data[24] = i;
	tx_data[23] = (i>>8);
}

int getImitActIdo(void)
{
	return ((tx_data[23]<<8)+tx_data[24]);
}

//Bites v�ltoz�k
int getFauzem(void)
{
	if ((getKazanType() == 1) && ((rx_data[43]&0X01) > 0)) return 1;
	else return 0;
}

int getMelegentart(void)
{
	if((rx_data[43]&0X02) > 0)return 1;
	else return 0;
}

int getRoomTherPresent(void)
{  
	if((rx_data[43]&0X04) > 0)return 1;
	else return 0;
}

int getFusthomeroPresent(void)
{  
	if((rx_data[43]&0X08) > 0)return 1;
	else return 0;
}

int getLowlevelEnable(void)
{
	if((rx_data[43]&0X20) > 0)return 1;
	else return 0;
}

int getBurninEnable(void)
{
	if((rx_data[43]&0X40) > 0)return 1;
	else return 0;
}

int getBurninAlarm(void)
{
	if((rx_data[43]&0X80) > 0)return 1;
	else return 0;
}

int getTulfutesAlarm(void)
{
	if((rx_data[44]&0X01) > 0)return 1;
	else return 0;
}

int getPumpPresent(void)
{
	if((rx_data[44]&0X02) > 0)return 1;
	else return 0;
}


int getThreephaseVentil(void)
{
	if((rx_data[44]&0X04) > 0)return 1;
	else return 0;
}

int getGozos(void)
{
	int i = getKazanType();
	if((i==5)||(i==6)||(i==8))
	{
		return 1;
	}
	else{return 0;}
}

int getBolygato230V(void)
{
	if((rx_data[44]&0X20) > 0)return 1;
	else return 0;
}

int getToltoInstalled(void)
{
	if((rx_data[44]&0X40) > 0)return 1;
	else return 0;
}

int getToltocsiga2Installed(void)
{
	if((rx_data[44]&0X80) > 0)return 1;
	else return 0;
}

int getSalakoloInstalled(void)
{
	if((rx_data[45]&0X01) > 0)return 1;
	else return 0;
}

int getFelhordoInstalled(void)
{
	if((rx_data[45]&0X02) > 0)return 1;
	else return 0;
}

int getFelhordocsiga2Installed(void)
{
	if((rx_data[45]&0X04) > 0)return 1;
	else return 0;
}

int getToltorendszerTiltas(void)
{
	if ((rx_data[45]&0X08) || getStopError()) return 1;
	else return 0;
}

int getSalakolorendszerTiltas(void)
{
	if((rx_data[45]&0X10) || getStopError()) return 1;
	else return 0;
}

int getKazanTiltas(void)
{
	if((rx_data[45]&0X20) > 0)return 1;
	else return 0;
}

int getSzelloztetes(void)
{
	if((rx_data[45]&0X40) > 0)return 1;
	else return 0;
}

int getTestmode(void)
{
	if((rx_data[45]&0X80) > 0)return 1;
	else return 0;
}

int getUresKulsoEseten(void)
{
	if((rx_data[46]&0X01) > 0)return 1;
	else return 0;
}

int getHomero3Present(void)
{
	if((rx_data[46]&0X02) > 0)return 1;
	else return 0;
}

int getHomero3Melegen(void)
{
	if((rx_data[46]&0X04) > 0)return 1;
	else return 0;
}

int getEkletraPresent(void)
{
	if((rx_data[46]&0X08) > 0)return 1;
	else return 0;
}

int getFelsofelegesPresent(void)
{
	if((rx_data[46]&0X10) > 0)return 1;
	else return 0;
}

int getError35Stop(void)
{
	if((rx_data[47]&0X01) > 0)return 1;
	else return 0;
}

int getError35Restart(void)
{
	if((rx_data[47]&0X02) > 0)return 1;
	else return 0;
}

int getError35SendSMS(void)
{
	if((rx_data[47]&0X04) > 0)return 1;
	else return 0;
}

int getError35Default(void)
{
	if((rx_data[47]&0X10) > 0)return 1;
	else return 0;
}

int getCsengokimenetFolyamatos(void)
{
	if((rx_data[47]&0X08) > 0)return 1;
	else return 0;
}

//Gombok

int getInitOK(void)
{
	if((rx_data[0]&0X01) > 0)return 1;
	else return 0;
}

int getKezi(void)
{
	if(KULSOKEZI){return 1;}
	if((rx_data[0]&0X02) > 0)return 1;
	else return 0;
}

int getRostelyGomb(void)
{
	if((rx_data[0]&0X04) > 0)return 1;
	else return 0;
}

int getVentiGomb(void)
{
	if((rx_data[0]&0X08) > 0)return 1;
	else return 0;
}

int getBolygGomb(void)
{
	if((rx_data[0]&0X10) > 0)return 1;
	else return 0;
}

int getBelimoGomb(void)
{
	if((rx_data[0]&0X20) > 0)return 1;
	else return 0;
}

int getSzivattyuGomb(void)
{
	if((rx_data[0]&0X40) > 0)return 1;
	else return 0;
}

int getSzivattyu2Gomb(void)
{
	if((rx_data[0]&0X80) > 0)return 1;
	else return 0;
}

int getClearGomb(void)
{
	if((rx_data[1]&0X01) > 0)return 1;
	else return 0;
}

int getCsengetes(void)
{
	if((rx_data[1]&0X02) > 0)return 1;
	else return 0;
}

int getKazanStop(void)
{
	if((rx_data[1]&0X08) > 0)return 1;
	else return 0;
}

int getKazanReset(void)
{
	if((rx_data[1]&0X10) > 0)return 1;
	else return 0;
}

void clearKazanStop(void)
{
	rx_data[1] &= 0Xe7;
}

int getRelAGomb(void)
{
	if((rx_data[102]&0X01) > 0)return 1;
	else return 0;
}
int getRelBGomb(void)
{
	if((rx_data[102]&0X02) > 0)return 1;
	else return 0;
}

int getNyitasGomb(void)
{
	if((rx_data[2]&0X01) > 0)return 1;
	else return 0;
}

int getZarasGomb(void)
{
	if((rx_data[2]&0X02) > 0)return 1;
	else return 0;
}

int getTeritoEloreGomb(void)
{
	if((rx_data[2]&0X04) > 0)return 1;
	else return 0;
}

int getTeritoHatraGomb(void)
{
	if((rx_data[2]&0X08) > 0)return 1;
	else return 0;
}

int getPernyeEloreGomb(void)
{
	if((rx_data[3]&0X02) > 0)return 1;
	else return 0;
}

int getPernyeHatraGomb(void)
{
	return 0;
}

int getPockoloEloreGomb(void)
{
	if((rx_data[3]&0X04) > 0)return 1;
	else return 0;
}

int getPockoloHatraGomb(void)
{
	return 0;
}

int getTolto1EloreGomb(void)
{
	if((rx_data[2]&0X10) > 0)return 1;
	else return 0;
}

int getTolto1HatraGomb(void)
{
	if((rx_data[2]&0X20) > 0)return 1;
	else return 0;
}

int getTolto2EloreGomb(void)
{
	if((rx_data[2]&0X40) > 0)return 1;
	else return 0;
}

int getTolto2HatraGomb(void)
{
	if((rx_data[2]&0X80) > 0)return 1;
	else return 0;
}

int getSalakoloEloreGomb(void)
{
	if((rx_data[3]&0X08) > 0)return 1;
	else return 0;
}

int getSalakoloHatraGomb(void)
{
	if((rx_data[3]&0X10) > 0)return 1;
	else return 0;
}

int getFelhordo1EloreGomb(void)
{
	if((rx_data[3]&0X20) > 0)return 1;
	else return 0;
}

int getFelhordo1HatraGomb(void)
{
	if((rx_data[3]&0X40) > 0)return 1;
	else return 0;
}

int getFelhordo2EloreGomb(void)
{
	if((rx_data[3]&0X80) > 0)return 1;
	else return 0;
}

int getFelhordo2HatraGomb(void)
{
	if((rx_data[4]&0X01) > 0)return 1;
	else return 0;
}

int getRazomotorGomb(void)
{
	if((rx_data[3]&0X01) > 0)return 1;
	else return 0;
}

int getEkletraMehetGomb(void)
{
	if((rx_data[4]&0X04) > 0)return 1;
	else return 0;
}

int getGyujtHiba(void)
{
	if((rx_data[1]&0X04) > 0)return 1;
	else return 0;
}



//�kl�tra ID70 Gombok
int getEkletraKeziGomb(void)
{
	if((rx_data[5]&0X01) > 0)return 1;
	else return 0;
}

int getHidrotapGomb(void)
{
	if((rx_data[5]&0X02) > 0)return 1;
	else return 0;
}

int getEkletraEloreGomb(void)
{
	if((rx_data[5]&0X04) > 0)return 1;
	else return 0;
}

int getEkletraHatraGomb(void)
{
	if((rx_data[5]&0X08) > 0)return 1;
	else return 0;
}

int getGyujtocsigaEloreGomb(void)
{
	if((rx_data[5]&0X10) > 0)return 1;
	else return 0;
}

int getGyujtocsigaHatraGomb(void)
{
	if((rx_data[5]&0X20) > 0)return 1;
	else return 0;
}

//--------------�kl�tra ID70 Gombok

int getEkletraTiltas(void)
{
	if((rx_data[6]&0X01) > 0)return 1;
	else return 0;
}

int setTime(void)
{
	if((rx_data[119]&0X01) > 0)return 1;
	else return 0;
}


//Hiba�zenetek

//Kaz�n kialudt hiba bekapcs
void setKihultError(void)
{
	tx_data[41] |= (1 << 0);
	setCANKazanKihult();
	setSMStuzkialudt();
}
//Kaz�n kialudt hiba kikapcs
void clearKihultError(void)
{
	tx_data[41] &= ~(1 << 0);
	clearCANKazanKihult();
	clearSMStuzkialudt();
}
//Kaz�n kialudt �llapot
int getKihultError(void)
{
	if((tx_data[41] & (1<< 0)) > 0) return 1;
	else return 0;
}

void setError35(void)
{
	tx_data[42] |= (1 << 3);
	if (getError35SendSMS()) setSMSerror35();
}

void clearError35(void)
{
	tx_data[42] &= ~(1 << 3);
	clearSMSerror35();
}

int getError35(void)
{
	if((tx_data[42] & (1<< 3)) > 0) return 1;
	else return 0;
}

void setError41(void)
{
	tx_data[42] |= (1 << 4);
	setSMSerror41();
}

void clearError41(void)
{
	tx_data[42] &= ~(1 << 4);
	clearSMSerror41();
}

int getError41(void)
{
	if((tx_data[42] & (1<< 4)) > 0) return 1;
	else return 0;
}

//Watchdog
void setWatchdogError(void)
{
	tx_data[41] |= (1 << 1);
}

void clearWatchdogError(void)
{
	tx_data[41] &= ~(1 << 1);
}

int getWatchdogError(void)
{
	if((tx_data[41] & (1<< 1)) > 0)return 1;
	else return 0;
}


//T�lf�t�s
void setTulfutes(void)
{
	tx_data[41] |= (1 << 3);
}

void clearTulfutes(void)
{
	tx_data[41] &= ~(1 << 3);
}

int getTulfutes(void)
{
	if((tx_data[41] & (1<< 3)) > 0)return 1;
	else return 0;
}


//Ventil�tor Motorv�d� lekapcsolt
void setVentilatorMVError(void)
{
	tx_data[42] |= (1 << 1);
}

void clearVentilatorMVError(void)
{
	tx_data[42] &= ~(1 << 1);
}

int getVentilatorMVError(void)
{
	if((tx_data[42] & (1<< 1)) > 0)return 1;
	else return 0;
}

//Bolygat� Motorv�d� lekapcsolt
void setBolygatoMVError(void)
{
	tx_data[41] |= (1 << 2);
}

void clearBolygatoMVError(void)
{
	tx_data[41] &= ~(1 << 2);
}

int getBolygatoMVError(void)
{
	if((tx_data[41] & (1<< 2)) > 0)return 1;
	else return 0;
}

//T�lmeleged�s
void setOverheatError(void)
{
	tx_data[42] |= (1 << 0);
	setSMStulmelegedes();
}

void clearOverheatError(void)
{
	tx_data[42] &= ~(1 << 0);
	clearSMStulmelegedes();
}

int getOverheatError(void)
{
	if((tx_data[42] & (1<< 0)) > 0)return 1;
	else return 0;
}

//Felhaszn�l� �llj
void setStopError(void)
{
	tx_data[42] |= (1 << 2);
	setSMSstopError();
}

void clearStopError(void)
{
	tx_data[42] &= ~(1 << 2);
	clearSMSstopError();
}

int getStopError(void)
{
	if((tx_data[42] & (1 << 2)) > 0)return 1;
	else return 0;
} 

//T�zolt�s
void setTuzoltasError(void)
{
	tx_data[41] |= (1 << 4);
}

void clearTuzoltasError(void)
{
	tx_data[41] &= ~(1 << 4);
}

int getTuzoltasError(void)
{
	if((tx_data[41] & (1<< 4)) > 0)return 1;
	else return 0;
}

//V�zszint alacsony
void setErrorVizszintAlacsony(void)
{
	tx_data[41] |= (1 << 5);
}

void clearErrorVizszintAlacsony(void)
{
	tx_data[41] &= ~(1 << 5);
}

int getErrorVizszintAlacsony(void)
{
	if((tx_data[41] & (1<< 5)) > 0)return 1;
	else return 0;
}

//Akku hi�nyzik
void setAkkuHianyzik(void)
{
	tx_data[41] |= (1 << 6);
}

void clearAkkuHianyzik(void)
{
	tx_data[41] &= ~(1 << 6);
}

int getAkkuHianyzik(void)
{
	if((tx_data[41] & (1<< 6)) > 0)return 1;
	else return 0;
}

//Akku gyenge
void setAkkuGyenge(void)
{
	tx_data[41] |= (1 << 7);
}

void clearAkkuGyenge(void)
{
	tx_data[41] &= ~(1 << 7);
}

int getAkkuGyenge(void)
{
	if((tx_data[41] & (1<< 7)) > 0)return 1;
	else return 0;
}

//T�lt�rendszer hiba�zenetei

//T�lt�csiga1 pr�b�lkozik
void setTolt1ProbalError(void)
{
	tx_data[43] |= (1 << 0);
}

void clearTolt1ProbalError(void)
{
	tx_data[43] &= ~(1 << 0);
}

int getTolt1ProbalError(void)
{
	if((tx_data[43] & (1<< 0)) > 0)return 1;
	else return 0;
}

//T�lt�csiga2 pr�b�lkozik
void setTolt2ProbalError(void)
{
	tx_data[43] |= (1 << 1);
}

void clearTolt2ProbalError(void)
{
	tx_data[43] &= ~(1 << 1);
}

int getTolt2ProbalError(void)
{
	if((tx_data[43] & (1<< 1)) > 0)return 1;
	else return 0;
}

//Ter�t�csiga pr�b�lkozik
void setTeritoProbalError(void)
{
	tx_data[43] |= (1 << 2);
}

void clearTeritoProbalError(void)
{
	tx_data[43] &= ~(1 << 2);
}

int getTeritoProbalError(void)
{
	if((tx_data[43] & (1<< 2)) > 0)return 1;
	else return 0;
}

//Felhord�csiga1 pr�b�lkozik
void setFelh1ProbalError(void)
{
	tx_data[43] |= (1 << 3);
}

void clearFelh1ProbalError(void)
{
	tx_data[43] &= ~(1 << 3);
}

int getFelh1ProbalError(void)
{
	if((tx_data[43] & (1<< 3)) > 0)return 1;
	else return 0;
}

//Felhord�csiga2 pr�b�lkozik
void setFelh2ProbalError(void)
{
	tx_data[43] |= (1 << 4);
}

void clearFelh2ProbalError(void)
{
	tx_data[43] &= ~(1 << 4);
}

int getFelh2ProbalError(void)
{
	if((tx_data[43] & (1<< 4)) > 0)return 1;
	else return 0;
}

//Salakol�csiga pr�b�lkozik
void setSalakProbalError(void)
{
	tx_data[43] |= (1 << 5);
}

void clearSalakProbalError(void)
{
	tx_data[43] &= ~(1 << 5);
}

int getSalakProbalError(void)
{
	if((tx_data[43] & (1<< 5)) > 0)return 1;
	else return 0;
}

//T�lt�csiga1 elakadt
void setTolt1ElakadtError(void)
{
	tx_data[43] |= (1 << 6);
}

void clearTolt1ElakadtError(void)
{
	tx_data[43] &= ~(1 << 6);
}

int getTolt1ElakadtError(void)
{
	if((tx_data[43] & (1<< 6)) > 0)return 1;
	else return 0;
}

//T�lt�csiga2 elakadt
void setTolt2ElakadtError(void)
{
	tx_data[43] |= (1 << 7);
}

void clearTolt2ElakadtError(void)
{
	tx_data[43] &= ~(1 << 7);
}

int getTolt2ElakadtError(void)
{
	if((tx_data[43] & (1<< 7)) > 0)return 1;
	else return 0;
}

//Ter�t�csiga elakadt
void setTeritoElakadtError(void)
{
	tx_data[44] |= (1 << 0);
}

void clearTeritoElakadtError(void)
{
	tx_data[44] &= ~(1 << 0);
}

int getTeritoElakadtError(void)
{
	if((tx_data[44] & (1<< 0)) > 0)return 1;
	else return 0;
}

//Felhord�csiga1 elakadt
void setFelh1ElakadtError(void)
{
	tx_data[44] |= (1 << 1);
}

void clearFelh1ElakadtError(void)
{
	tx_data[44] &= ~(1 << 1);
}

int getFelh1ElakadtError(void)
{
	if((tx_data[44] & (1<< 1)) > 0)return 1;
	else return 0;
}

//Felhord�csiga2 elakadt
void setFelh2ElakadtError(void)
{
	tx_data[44] |= (1 << 2);
}

void clearFelh2ElakadtError(void)
{
	tx_data[44] &= ~(1 << 2);
}

int getFelh2ElakadtError(void)
{
	if((tx_data[44] & (1<< 2)) > 0)return 1;
	else return 0;
}

//Salakol�csiga elakadt
void setSalakElakadtError(void)
{
	tx_data[44] |= (1 << 3);
}

void clearSalakElakadtError(void)
{
	tx_data[44] &= ~(1 << 3);
}

int getSalakElakadtError(void)
{
	if((tx_data[44] & (1<< 3)) > 0)return 1;
	else return 0;
}

//P�ck�l�csiga elakadt
void setPockoloElakadtError(void)
{
	tx_data[44] |= (1 << 4);
}

void clearPockoloElakadtError(void)
{
	tx_data[44] &= ~(1 << 4);
}

int getPockoloElakadtError(void)
{
	if((tx_data[44] & (1<< 4)) > 0)return 1;
	else return 0;
}

//Pernyecsiga elakadt
void setPernyeElakadtError(void)
{
	tx_data[44] |= (1 << 5);
}

void clearPernyeElakadtError(void)
{
	tx_data[44] &= ~(1 << 5);
}

int getPernyeElakadtError(void)
{
	if((tx_data[44] & (1<< 5)) > 0)return 1;
	else return 0;
}


//T�lt�csiga1 Motorv�d� nem kapcsolt vissza
void setTolt1MVnemkapcsoltError(void)
{
	tx_data[44] |= (1 << 6);
}

void clearTolt1MVnemkapcsoltError(void)
{
	tx_data[44] &= ~(1 << 6);
}

int getTolt1MVnemkapcsoltError(void)
{
	if((tx_data[44] & (1<< 6)) > 0)return 1;
	else return 0;
}

//T�lt�csiga2 Motorv�d� nem kapcsolt vissza
void setTolt2MVnemkapcsoltError(void)
{
	tx_data[44] |= (1 << 7);
}

void clearTolt2MVnemkapcsoltError(void)
{
	tx_data[44] &= ~(1 << 7);
}

int getTolt2MVnemkapcsoltError(void)
{
	if((tx_data[44] & (1<< 7)) > 0)return 1;
	else return 0;
}

//Ter�t�csiga Motorv�d� nem kapcsolt vissza
void setTeritoMVnemkapcsoltError(void)
{
	tx_data[45] |= (1 << 0);
}

void clearTeritoMVnemkapcsoltError(void)
{
	tx_data[45] &= ~(1 << 0);
}

int getTeritoMVnemkapcsoltError(void)
{
	if((tx_data[45] & (1<< 0)) > 0)return 1;
	else return 0;
}

//Felhord�csiga1 Motorv�d� nem kapcsolt vissza
void setFelh1MVnemkapcsoltError(void)
{
	tx_data[45] |= (1 << 1);
}

void clearFelh1MVnemkapcsoltError(void)
{
	tx_data[45] &= ~(1 << 1);
}

int getFelh1MVnemkapcsoltError(void)
{
	if((tx_data[45] & (1<< 1)) > 0)return 1;
	else return 0;
}

//Felhord�csiga2 Motorv�d� nem kapcsolt vissza
void setFelh2MVnemkapcsoltError(void)
{
	tx_data[45] |= (1 << 2);
}

void clearFelh2MVnemkapcsoltError(void)
{
	tx_data[45] &= ~(1 << 2);
}

int getFelh2MVnemkapcsoltError(void)
{
	if((tx_data[45] & (1<< 2)) > 0)return 1;
	else return 0;
}

//Salakol�csiga Motorv�d� nem kapcsolt vissza
void setSalakMVnemkapcsoltError(void)
{
	tx_data[45] |= (1 << 3);
}

void clearSalakMVnemkapcsoltError(void)
{
	tx_data[45] &= ~(1 << 3);
}

int getSalakMVnemkapcsoltError(void)
{
	if((tx_data[45] & (1<< 3)) > 0)return 1;
	else return 0;
}

//T�lt�ajt� pr�b�lkozik
void setAjtoProbalError(void)
{
	tx_data[45] |= (1 << 4);
}

void clearAjtoProbalError(void)
{
	tx_data[45] &= ~(1 << 4);
}

int getAjtoProbalError(void)
{
	if((tx_data[45] & (1<< 4)) > 0)return 1;
	else return 0;
}

//T�lt�ajt� nem nyitott
void setAjtoNemnyitottError(void)
{
	tx_data[45] |= (1 << 5);
}

void clearAjtoNemnyitottError(void)
{
	tx_data[45] &= ~(1 << 5);
}

int getAjtoNemnyitottError(void)
{
	if((tx_data[45] & (1<< 5)) > 0)return 1;
	else return 0;
}

//T�lt�ajt� nem z�rt
void setAjtoNemzartError(void)
{
	tx_data[45] |= (1 << 6);
}

void clearAjtoNemzartError(void)
{
	tx_data[45] &= ~(1 << 6);
}

int getAjtoNemzartError(void)
{
	if((tx_data[45] & (1<< 6)) > 0)return 1;
	else return 0;
}

//T�lt�ajt� nyitva kapcsol� rossz
void setAjtoNyitvakapcsoloRossz(void)
{
	tx_data[45] |= (1 << 7);
}

void clearAjtoNyitvakapcsoloRossz(void)
{
	tx_data[45] &= ~(1 << 7);
}

int getAjtoNyitvakapcsoloRossz(void)
{
	if((tx_data[45] & (1<< 7)) > 0)return 1;
	else return 0;
}

//T�lt�ajt� z�rva kapcsol� rossz
void setAjtoZarvakapcsoloRossz(void)
{
	tx_data[46] |= (1 << 0);
}

void clearAjtoZarvakapcsoloRossz(void)
{
	tx_data[46] &= ~(1 << 0);
}

int getAjtoZarvakapcsoloRossz(void)
{
	if((tx_data[46] & (1<< 0)) > 0)return 1;
	else return 0;
}

//Szintjelz� hiba
void setSzintjelzoHiba(void)
{
	tx_data[46] |= (1 << 1);
}

void clearSzintjelzoHiba(void)
{
	tx_data[46] &= ~(1 << 1);
}

int getSzintjelzoHiba(void)
{
	if((tx_data[46] & (1<< 1)) > 0)return 1;
	else return 0;
}

//Id�t�ll�p�s hiba
void setIdotullepesHiba(void)
{
	tx_data[46] |= (1 << 2);
}

void clearIdotullepesHiba(void)
{
	tx_data[46] &= ~(1 << 2);
}

int getIdotullepesHiba(void)
{
	if((tx_data[46] & (1<< 2)) > 0)return 1;
	else return 0;
}

//P�ck�l�motor pr�b�lkozik
void setPockoloProbalError(void)
{
	tx_data[46] |= (1 << 3);
}

void clearPockoloProbalError(void)
{
	tx_data[46] &= ~(1 << 3);
}

int getPockoloProbalError(void)
{
	if((tx_data[46] & (1<< 3)) > 0)return 1;
	else return 0;
}

//Pernyecsiga pr�b�lkozik
void setPernyeProbalError(void)
{
	tx_data[46] |= (1 << 4);
}

void clearPernyeProbalError(void)
{
	tx_data[46] &= ~(1 << 4);
}

int getPernyeProbalError(void)
{
	if((tx_data[46] & (1<< 4)) > 0)return 1;
	else return 0;
}

//Salakajt� nyitva
void setErrorSalakajtoNyitva(void)
{
	tx_data[46] |= (1 << 5);
}

void clearErrorSalakajtoNyitva(void)
{
	tx_data[46] &= ~(1 << 5);
}

int getErrorSalakajtoNyitva(void)
{
	if((tx_data[46] & (1<< 5)) > 0)return 1;
	else return 0;
}

//B�v�t� �ramsz�net
void setBovitoAramszunetError(void)
{
	tx_data[46] |= (1 << 6);
}

void clearBovitoAramszunetError(void)
{
	tx_data[46] &= ~(1 << 6);
}

int getBovitoAramszunetError(void)
{
	if((tx_data[46] & (1<< 6)) > 0)return 1;
	else return 0;
}

//K�ls� t�rol� �res
void setKulsoTartalyUresError(void)
{
	tx_data[46] |= (1 << 7);
}

void clearKulsoTartalyUresError(void)
{
	tx_data[46] &= ~(1 << 7);
}

int getKulsoTartalyUresError(void)
{
	if((tx_data[46] & (1<< 7)) > 0)return 1;
	else return 0;
}

//ID70 �kl�tra hiba�zenetek
//�kl�tra id�t�ll�p�s
void setEkletraIdotullepesError(void)
{
	tx_data[48] |= (1 << 0);
}

void clearEkletraIdotullepesError(void)
{
	tx_data[48] &= ~(1 << 0);
}

int getEkletraIdotullepesError(void)
{
	if((tx_data[48] & (1<< 0)) > 0)return 1;
	else return 0;
}

//Gy�jt�csiga 1 pr�b�lkozik
void setGyujto1ProbalError(void)
{
	tx_data[48] |= (1 << 1);
}

void clearGyujto1ProbalError(void)
{
	tx_data[48] &= ~(1 << 1);
}

int getGyujto1ProbalError(void)
{
	if((tx_data[48] & (1<< 1)) > 0)return 1;
	else return 0;
}

//Gy�jt�csiga 1 elakadt
void setGyujto1ElakadtError(void)
{
	tx_data[48] |= (1 << 2);
}

void clearGyujto1ElakadtError(void)
{
	tx_data[48] &= ~(1 << 2);
}

int getGyujto1ElakadtError(void)
{
	if((tx_data[48] & (1<< 2)) > 0)return 1;
	else return 0;
}

//T�lt�s megy
void setToltesmegy(void)
{
	tx_data[29] |= (1 << 0);
}

void clearToltesmegy(void)
{
	tx_data[29] &= ~(1 << 0);
}

int getToltesmegy(void)
{
	if((tx_data[29] & (1<< 0)) > 0)return 1;
	else return 0;
}

//Salakol�s megy
void setSalakolasmegy(void)
{
	tx_data[29] |= (1 << 1);
}

void clearSalakolasmegy(void)
{
	tx_data[29] &= ~(1 << 1);
}

int getSalakolasmegy(void)
{
	if((tx_data[29] & (1<< 1)) > 0)return 1;
	else return 0;
}

//Felk�sz�l�s g�z�zemre
void setGozfelkeszul(void)
{
	tx_data[29] |= (1 << 2);
}

void clearGozfelkeszul(void)
{
	tx_data[29] &= ~(1 << 2);
}

int getGozfelkeszul(void)
{
	if((tx_data[29] & (1<< 2)) > 0)return 1;
	else return 0;
}

//Demo m�d
void setDemo(void)
{
	tx_data[42] |= (1 << 7);
}

void clearDemo(void)
{
	tx_data[42] &= ~(1 << 7);
}

int getDemo(void)
{
	if((tx_data[42] & (1<< 7)) > 0)return 1;
	else return 0;
}

void setUzemoraTimer(int i)
{
	tx_data[25] = i;
}

int getUzemoraTimer(void)
{
	return tx_data[25];
}

void incUzemoraTimer(void)
{
	tx_data[25] = tx_data[25] + 1;
}

int getMaxUzemora(void)
{
	return rx_data[101];
}

int getUzemoraNullazas(void)
{
	if((rx_data[119] & (1<< 1)) > 0)return 1;
	else return 0;
}

unsigned char get_relay_state(int idx)
{
	return rx_data[103+idx];
}
