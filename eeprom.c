#include "AT91SAM7X256.H"


void twiWait(void)
{
    int twiX;
    for(twiX=0;twiX<5000000;twiX++);
}

void TWI_init()
{
	AT91C_BASE_TWI->TWI_CWGR = 0X0007FFFF;
  //AT91C_BASE_TWI->TWI_CR |= AT91C_TWI_MSEN;
}//init

void readEeprom(unsigned char arr[],int address,int length)
{
	unsigned int status;
	int TWIptr = 0;

	AT91C_BASE_TWI->TWI_CR = AT91C_TWI_MSEN ;
	AT91C_BASE_TWI->TWI_MMR = 0X00501200;
	AT91C_BASE_TWI->TWI_IADR = address;
	AT91C_BASE_TWI->TWI_CR |= AT91C_TWI_START;

	while(TWIptr < (length-1))
	{	
    status = AT91C_BASE_TWI->TWI_SR;   
    while (!(status & AT91C_TWI_RXRDY)){status = AT91C_BASE_TWI->TWI_SR;}
		arr[TWIptr] = AT91C_BASE_TWI->TWI_RHR;
		TWIptr++;
	}
	AT91C_BASE_TWI->TWI_CR |= AT91C_TWI_STOP;
	status = AT91C_BASE_TWI->TWI_SR;   
   	while (!(status & AT91C_TWI_RXRDY)){status = AT91C_BASE_TWI->TWI_SR;}
	 
	arr[TWIptr] = AT91C_BASE_TWI->TWI_RHR;
	 
	status = AT91C_BASE_TWI->TWI_SR;   
  	while (!(status & AT91C_TWI_TXCOMP)){status = AT91C_BASE_TWI->TWI_SR;}   
}//read

void writeEeprom(unsigned char arr[],int address,int length)
{
	unsigned int status;
	int TWIptr = 0;
	
	AT91C_BASE_TWI->TWI_CR = AT91C_TWI_MSEN ;
	AT91C_BASE_TWI->TWI_MMR = 0X00500200;
	AT91C_BASE_TWI->TWI_IADR = address;	
	
	status = AT91C_BASE_TWI->TWI_SR;   
  while (!(status & AT91C_TWI_TXRDY)){status = AT91C_BASE_TWI->TWI_SR;}
	
	while(TWIptr < length)
	{
	 	AT91C_BASE_TWI->TWI_THR = arr[TWIptr];
		status = AT91C_BASE_TWI->TWI_SR;   
		while (!(status & AT91C_TWI_TXRDY)){status = AT91C_BASE_TWI->TWI_SR;}
		TWIptr++;
	}
	
	status = AT91C_BASE_TWI->TWI_SR;   
	while (!(status & AT91C_TWI_TXCOMP)){status = AT91C_BASE_TWI->TWI_SR;}
	
	twiWait();
}//write

