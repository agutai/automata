#include "AT91SAM7X256.H"
#include "csiga.h"
#include "Tolto.h"
#include "Board.h"
#include "data.h"
#include "expander.h"



void csigaTimer(volatile csiga_ *cst)
{
	cst->felfutTimer++;
	cst->motorvedoTimer++;
}


int csiga(volatile csiga_ *cs)
{
	cs->forgTimer++;
	if(cs->Motorvedo())				//motorv�d� visszakapcsolt
	{
		cs->MV = 1;
	}
	else
	{
		cs->MV = 0;
	}						//motorv�d� lekapcsolt

	if(getClearGomb())				//Hibat�rl�s
	{
		cs->proba_counter = 0;
		cs->probalkozik = 0;
	 	cs->elakadt = 0;
	}

	if(getKezi()&&(cs->MV))				//K�zi
	{
	 	if(cs->get_eloregomb())
		{
			if((cs->kezistart == 0)&& !cs->aramkorlat_lekapcsolt)
			{
				cs->Allj();
				cs->kezistart = 1;
				cs->felfutTimer = 0;
			}

			if((cs->felfutTimer > START_SZUNET) && !cs->aramkorlat_lekapcsolt)	//Sz�net indul�s el�tt
			{	
				cs->Elore();
			}

			if(cs->aramfigyeles && (cs->felfutTimer > (START_SZUNET + 5)))		//Ha van �ramfelv�tel figyel�s
			{
			 	if(cs->get_arammero() > cs->get_aramkorlat())			//Ha t�l nagy az �ramfelv�tel
				{
					cs->aramkorlat_lekapcsolt = 1;
				 	cs->Allj();						//�llj, elakadt
				}
			}

		}
		if(!cs->get_eloregomb()){cs->kezistart = 0;cs->aramkorlat_lekapcsolt = 0;}

		if(cs->get_hatragomb()){cs->Hatra();}
		if((!cs->get_eloregomb()) && (!cs->get_hatragomb())){cs->Allj();}

		cs->proba_counter = 0;
		cs->probalkozik = 0;
	 	cs->elakadt = 0;
		cs->start = 0;

		cs->error_state = CSIGA_OK;
	}  //K�zi

	if(!getKezi() &&(cs->elakadt == 0))							//Automata	vagy f�lautomata
	{
		cs->kezistart = 0;

		if(!cs->mehet)
		{
			cs->start = 0;
			cs->Allj();
			cs->error_state = CSIGA_NOT_OK;
			cs->probalkozik = 0;
			//cs->proba_counter = 0;
		}

		if((cs->mehet == 1)&&(cs->probalkozik == 0)&&(cs->elakadt == 0)&&(cs->MV == 1))
		{
			if(cs->start == 0)					//Norm�l el�remenet indul�s
			{
				cs->start = 1;
				cs->felfutTimer = 0;
				cs->felfutas = 1;

				cs->error_state = CSIGA_NOT_OK;
			}

			if(cs->felfutTimer > START_SZUNET)			//Sz�net indul�s el�tt
			{
				cs->Elore();
			}

			if(cs->felfutTimer > 10+START_SZUNET)
			{
				cs->felfutas = 0;				//Felfut�s id� letelt
			}

			if(cs->felfutTimer > (10+START_SZUNET+ELAKADASMENTES))
			{
			 	cs->proba_counter = 0;				//Elakad�smentes el�remenet
			}

			if(cs->Forgasfigyelo())					//Forg�sfigyel� bekapcsolva
			{
				if(cs->forg_state == 0)				//Felfut� �l
				{
					cs->forg_state = 1;
					cs->forgTimer = 0;
				}
			}

			if(!(cs->Forgasfigyelo()))				//Forg�sfigyel� kikapcsolva
			{
			 	cs->forg_state = 0;
			}

			if(cs->aramfigyeles)					//Ha van �ramfelv�tel figyel�s
			{
			 	if(cs->get_arammero() > cs->get_aramkorlat())		//Ha t�l nagy az �ramfelv�tel
				{
				 	cs->Allj();					//�llj, elakadt
					cs->start = 0;
			 		cs->probalkozik = 1;
					cs->proba_counter++;
				}
			}

			if((cs->felfutas == 0)&&(cs->forgTimer > (cs->get_forgido() * (cs->forgido_mul))))
			{
				cs->Allj();						//�llj, elakadt
				cs->start = 0;
			 	cs->probalkozik = 1;
				cs->proba_counter++;
			}

		}	//norm�l el�remenet

		if((cs->mehet == 1)&&(cs->probalkozik == 1)&&(cs->elakadt == 0)&&(cs->MV))	//Ha pr�b�lkozik
		{
		 	if(cs->start == 0)
			{
				cs->start = 1;
				cs->felfutTimer = 0;
			}

			if(cs->felfutTimer > START_SZUNET)
			{
				cs->Hatra();							//H�tra
			}

			if(cs->aramfigyeles && (cs->felfutTimer > (START_SZUNET + 5)))		//Ha van �ramfelv�tel figyel�s
			{
				if(cs->get_arammero() > cs->get_aramkorlat())			//Ha t�l nagy az �ramfelv�tel
				{
					cs->Allj();						//�llj, elakadt
					cs->start = 0;
					cs->probalkozik = 0;
				}
			}

			if(cs->felfutTimer > START_SZUNET+HATRAMENET)
			{
				cs->Allj();
				cs->probalkozik= 0;
				cs->start = 0;
			}
		}	//Pr�b�lkoz�s, h�tramenet

	}		//Automata

	if(cs->probalkozik == 1){cs->error_state = CSIGA_PROBALKOZIK;}
	if(cs->proba_counter > getProbalkozasSzam()){cs->elakadt = 1;cs->error_state = CSIGA_ELAKADT;}
	if((cs->start==1) && (cs->felfutTimer > START_SZUNET + 10) && (cs->proba_counter == 0 )){cs->error_state = CSIGA_OK;}

	if(!cs->MV && BOVITO_LEKAPCS)	  							//Motorv�d� lekapcsolt
	{
		cs->start = 0;
		cs->error_state = CSIGA_NOT_OK;
		cs->probalkozik = 0;
		cs->Allj();									//�llj

		if(cs->motorvedo_start == 0)
		{
			cs->motorvedo_start = 1;
			cs->motorvedoTimer = 0;
		}

		if(cs->motorvedoTimer > MOTORVEDO_IDO)					//Ha hossz� id� ut�n sem kapcsol vissza
		{
			cs->MV_nemkapcsolt = 1;
		}
	}

	if(cs->MV)									//Motorv�d� visszakapcsolt
	{
	 	cs->motorvedo_start = 0;
		cs->MV_nemkapcsolt = 0;
	}

	if(!cs->MV&& BOVITO_LEKAPCS){cs->error_state = MV_LEKAPCSOLT;}
	if(cs->MV_nemkapcsolt){cs->error_state = MV_NEMKAPCSOLT_VISSZA;}

	return cs->error_state;
}
