#include "AT91SAM7X256.H"
#include "Board.h"
#include "data.h"

int clkState=0;

void ds_wait()
{
 	int x;
    for(x=0;x<5;x++);
}

void clockPulse()
{
	RTC_CLK_HI;ds_wait();RTC_CLK_LO;ds_wait(); 	
}

int BCD2Int(int i,int year)
{
	int x, y, upper, lower;
	y = i;
	lower = (i &= 0X0F);
	if(!year){x = (y &= 0X70);}
	else{x = (y &= 0XF0);} 
	upper = ((x >> 4)*10);
	x = upper + lower;
	return x;
}

int Int2BCD(int i)
{
	int x, y, upper, lower;
	y = i;
	lower = (i%10);
	upper = (y/10);
	y = (upper << 4);
	x = (y | lower);
	return x;	 	
}


int readDS3234(int addr)
{
	int data,x,y;
	
	RTC_DS_LO;ds_wait();RTC_CLK_LO;clkState=0;ds_wait();

	for(x=0;x<8;x++)
	{
		y=7-x;
		
	 	if((addr & (1<<y)) > 0){RTC_DIN_HI;}
		else {RTC_DIN_LO;}
		clockPulse();	
	}

	RTC_DIN_LO;
	data = 0;
			
	for(x=0;x<8;x++)
	{
		y=7-x;
		RTC_CLK_HI;ds_wait();
		if(RTC_DOUT > 0){data |= (1 << y);}
		else {data &= ~(1 << y);}
        RTC_CLK_LO;ds_wait();

	}

	RTC_CLK_LO;
	RTC_DS_HI;
	return data;			 	
}


void writeDS3234(int addr, int data)
{
	int x,y;
	
	RTC_DS_LO;ds_wait();RTC_CLK_LO;clkState=0;ds_wait();

	for(x=0;x<8;x++)
	{
		y=7-x;
		
	 	if((addr & (1<<y)) > 0){RTC_DIN_HI;}
		else {RTC_DIN_LO;}
		clockPulse();	
	}
	RTC_DIN_LO;
	
	for(x=0;x<8;x++)
	{
		y=7-x;
		if((data & (1<<y)) > 0){RTC_DIN_HI;}
		else {RTC_DIN_LO;}
		clockPulse();

	}
	RTC_CLK_LO;
	RTC_DIN_LO;
	RTC_DS_HI;			 	
}

int getSec(void)
{
 	int x;
	x = BCD2Int(readDS3234(0),0);
	return x;
}

void setSec(int i)
{
 	int x;
	x = Int2BCD(i);
	writeDS3234(128,x);
}

int getMin(void)
{
 	int x;
	x = BCD2Int(readDS3234(1),0);
	return x;
}

void setMin(int i)
{
 	int x;
	x = Int2BCD(i);
	writeDS3234(129,x);
}

int getHour(void)
{
 	int x;
	x = BCD2Int(readDS3234(2),0);;
	return x;
}

void setHour(int i)
{
 	int x;
	x = Int2BCD(i);
	writeDS3234(130,x);
}

int getDate(void)
{
 	int x;
	x = BCD2Int(readDS3234(4),0);;
	return x;
}

void setDate(int i)
{
 	int x;
	x = Int2BCD(i);
	writeDS3234(132,x);
}

int getMonth(void)
{
 	int x;
	x = BCD2Int(readDS3234(5),0);;
	return x;
}

void setMonth(int i)
{
 	int x;
	x = Int2BCD(i);
	writeDS3234(133,x);
}

int getYear(void)
{
 	int x;
	x = BCD2Int(readDS3234(6),1);;
	return x;
}

void setYear(int i)
{
 	int x;
	x = Int2BCD(i);
	writeDS3234(134,x);
}


