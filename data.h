#ifndef __DATA_H
#define __DATA_H

void set_tx_Data(unsigned char[], int);
void set_rx_Data(unsigned char[], int);

void loadData(void);
void saveData(void);

// ds3234

int getSec(void);
void setSec(int);
int getMin(void);
void setMin(int);
int getHour(void);
void setHour(int);
int getDate(void);
void setDate(int);
int getMonth(void);
void setMonth(int);
int getYear(void);
void setYear(int);

void settingTime(void);
void setVersion(int x, int y);

//01
int getKazanType(void);
int getHofok1(void);
int getHofok3(void);
int getVizhoMax(void);
int getVizhoKomp(void);
int getGoznyomas(void);
int getGoznyomasKomp(void);
int getVizhoHyst(void);
int getRostelyseb(void);
int getUzemiBolyg(void);
int getAlloBolyg(void);
int getBolygatoAramkorlat(void);
int getTulfutHyst(void);
int getKihulHyst(void);
int getKihulIdo(void);
int getTimerBe(void);
int getTimerSzunet(void);
int getTuzoltasTime(void);

int getFelsoNyomasHyst(void);
int getAlsoNyomasTav(void);
int getFigyelmeztetesiHofok(void);


//02
int getImitaltToltes(void);
int getIdotullepes(void);
int getProbalkozasSzam(void);
int getToltoajtoAramkorlat(void);
int getPernyecsigaAramkorlat(void);
int getPockoloAramkorlat(void);
int getPernyezoForgido(void);
int getPockoloForgido(void);
int getSalakoloForgido(void);
int getTolto1Forgido(void);
int getTolto2Forgido(void);
int getFelhordo1Forgido(void);
int getFelhordo2Forgido(void);
int getTeritoForgido(void);
int getSalakBe(void);
int getSalakSzunet(void);
int getPernyeFelfutas(void);
int getPockoloFelfutas(void);
int getSalakoloFelfutas(void);
int getTolto1Felfutas(void);
int getTolto2Felfutas(void);
int getFelhordo1Felfutas(void);
int getFelhordo2Felfutas(void);
int getTeritoFelfutas(void);
int getFelhordoUtanfutas(void);
int getKazanNum(void);
int getToltesSebesseg(void);
int getRazomotorSzunet(void);
int getRazomotorBe(void);
int getToltesSzunet(void);
int getToltesBe(void);

int getMaxUzemora(void);
int getDemoClear(void);
int getUzemoraNullazas(void);

int getEkletraIdotullepes(void);
int getEkletraElakadasinyomas(void);
int getEkletraMaxnyomas(void);
int getGyujto1Forgido(void);
int getGyujto2Forgido(void);


//bites v�ltoz�k
int getFauzem(void);
int getMelegentart(void);
int getRoomTherPresent(void);
int getFusthomeroPresent(void);
int getLowlevelEnable(void);
int getBurninEnable(void);
int getBurninAlarm(void);
int getTulfutesAlarm(void);
int getPumpPresent(void);
int getScheduleMode(void);
int getThreephaseVentil(void);
int getGozos(void);
int getBolygato230V(void);
int getToltoInstalled(void);
int getToltocsiga2Installed(void);
int getSalakoloInstalled(void);
int getFelhordoInstalled(void);
int getFelhordocsiga2Installed(void);
int getToltorendszerTiltas(void);
int getSalakolorendszerTiltas(void);
int getKazanTiltas(void);
int getSzelloztetes(void);
int getFelsofelegesPresent(void);
int getUresKulsoEseten(void);
int getError35Stop(void);
int getError35Restart(void);
int getError35SendSMS(void);
int getError35Default(void);
int getCsengokimenetFolyamatos(void);



//Gombok
int getKezi(void);
int getRostelyGomb(void);
int getVentiGomb(void);
int getBolygGomb(void);
int getBelimoGomb(void);
int getSzivattyuGomb(void);
int getSzivattyu2Gomb(void);
int getClearGomb(void);
int getCsengetes(void);
int getKazanStop(void);
int getKazanReset(void);
void clearKazanStop(void);
int getRelAGomb(void);
int getRelBGomb(void);
int getSheduleStart(void);
int getNyitasGomb(void);
int getZarasGomb(void);
int getTeritoEloreGomb(void);
int getTeritoHatraGomb(void);
int getPernyeEloreGomb(void);
int getPernyeHatraGomb(void);
int getPockoloEloreGomb(void);
int getPockoloHatraGomb(void);
int getTolto1EloreGomb(void);
int getTolto1HatraGomb(void);
int getTolto2EloreGomb(void);
int getTolto2HatraGomb(void);
int getSalakoloEloreGomb(void);
int getSalakoloHatraGomb(void);
int getFelhordo1EloreGomb(void);
int getFelhordo1HatraGomb(void);
int getFelhordo2EloreGomb(void);
int getFelhordo2HatraGomb(void);
int getRazomotorGomb(void);
int getEkletraMehetGomb(void);

int getEkletraKeziGomb(void);
int getHidrotapGomb(void);
int getEkletraEloreGomb(void);
int getEkletraHatraGomb(void);
int getGyujtocsigaEloreGomb(void);
int getGyujtocsigaHatraGomb(void);

int getEkletraTiltas(void);

int getFelautomata(void);
int getAutomata(void);


int setTime(void);

//Bemenetek
void setInputPin(int,int);
void clearInputPin(int,int);

//Kimenetek
void setOutputPin(int,int);
void clearOutputPin(int,int);

int getBolygBe(void);
void setBolygBe(int);

void refreshData(void);
void setHomero(int,int);
int getHomero1(void);
int getHomero2(void);
int getHomero3(void);
void setArammero(int);
int getArammero(void);
void setBolygatoArammero(int);
int getBolygatoArammero(void);
void setNyomasmero(unsigned char[]);
int getNyomasmero(void);
void setGozNyomasmero(int);
int getGozNyomasmero(void);
void setHidraulikaNyomasmero(unsigned char[]);
int getHidraulikaNyomasmero(void);
void setTuzterNyomasmero(unsigned char buff[]);
int getTuzterNyomasmero(void);
void setToltoajtoArammero(int);
int getToltoajtoArammero(void);
void setSalakajtoArammero(int);
int getSalakajtoArammero(void);
void setPernyeajtoArammero(int);
int getPernyeajtoArammero(void);

void setSalakActIdo(int);
void setSalakszunetActIdo(int);
void setImitActIdo(int);
int getImitActIdo(void);

void setGozfelkeszul(void);
void clearGozfelkeszul(void);
int getGozfelkeszul(void);

void setToltesmegy(void);
void clearToltesmegy(void);
int getToltesmegy(void);

void setSalakolasmegy(void);
void clearSalakolasmegy(void);
int getSalakolasmegy(void);

void setDemo(void);
void clearDemo(void);
int  getDemo(void);

void setUzemoraTimer(int);
int getUzemoraTimer(void);
void incUzemoraTimer(void);


//Hiba�zenetek
void setKihultError(void);
void clearKihultError(void);
int getKihultError(void);

void setWatchdogError(void);
void clearWatchdogError(void);
int getWatchdogError(void);

void setOverheatError(void);
void clearOverheatError(void);
int getOverheatError(void);

void setTulfutes(void);
void clearTulfutes(void);
int getTulfutes(void);

void setStopError(void);
void clearStopError(void);
int getStopError(void);

void setError35(void);
void clearError35(void);
int getError35(void);

void setError41(void);
void clearError41(void);
int getError41(void);

void setErrorVizszintAlacsony(void);
void clearErrorVizszintAlacsony(void);
int getErrorVizszintAlacsony(void);

void setTuzoltasError(void);
void clearTuzoltasError(void);
int getTuzoltasError(void);

void setAkkuNemOK(void);
void clearAkkuNemOK(void);
int getAkkuNemOK(void);

void setAkkuHianyzik(void);
void clearAkkuHianyzik(void);
int getAkkuHianyzik(void);

void setAkkuGyenge(void);
void clearAkkuGyenge(void);
int getAkkuGyenge(void);

void setVentilatorMVError(void);
void clearVentilatorMVError(void);
int getVentilatorMVError(void);

void setBolygatoMVError(void);
void clearBolygatoMVError(void);
int getBolygatoMVError(void);

void setTolt1ProbalError(void);
void clearTolt1ProbalError(void);
int getTolt1ProbalError(void);

void setTolt2ProbalError(void);
void clearTolt2ProbalError(void);
int getTolt2ProbalError(void);

void setTeritoProbalError(void);
void clearTeritoProbalError(void);
int getTeritoProbalError(void);

void setFelh1ProbalError(void);
void clearFelh1ProbalError(void);
int getFelh1ProbalError(void);

void setFelh2ProbalError(void);
void clearFelh2ProbalError(void);
int getFelh2ProbalError(void);

void setSalakProbalError(void);
void clearSalakProbalError(void);
int getSalakProbalError(void);

void setTolt1ElakadtError(void);
void clearTolt1ElakadtError(void);
int getTolt1ElakadtError(void);

void setTolt2ElakadtError(void);
void clearTolt2ElakadtError(void);
int getTolt2ElakadtError(void);

void setTeritoElakadtError(void);
void clearTeritoElakadtError(void);
int getTeritoElakadtError(void);

void setFelh1ElakadtError(void);
void clearFelh1ElakadtError(void);
int getFelh1ElakadtError(void);

void setFelh2ElakadtError(void);
void clearFelh2ElakadtError(void);
int getFelh2ElakadtError(void);

void setSalakElakadtError(void);
void clearSalakElakadtError(void);
int getSalakElakadtError(void);

void setPockoloElakadtError(void);
void clearPockoloElakadtError(void);
int getPockoloElakadtError(void);

void setPernyeElakadtError(void);
void clearPernyeElakadtError(void);
int getPernyeElakadtError(void);


void setTolt1MVError(void);
void clearTolt1MVError(void);
int getTolt1MVError(void);

void setTolt2MVError(void);
void clearTolt2MVError(void);
int getTolt2MVError(void);

void setTeritoMVError(void);
void clearTeritoMVError(void);
int getTeritoMVError(void);

void setFelh1MVError(void);
void clearFelh1MVError(void);
int getFelh1MVError(void);

void setFelh2MVError(void);
void clearFelh2MVError(void);
int getFelh2MVError(void);

void setSalakMVError(void);
void clearSalakMVError(void);
int getSalakMVError(void);



void setTolt1MVnemkapcsoltError(void);
void clearTolt1MVnemkapcsoltError(void);
int getTolt1MVnemkapcsoltError(void);

void setTolt2MVnemkapcsoltError(void);
void clearTolt2MVnemkapcsoltError(void);
int getTolt2MVnemkapcsoltError(void);

void setTeritoMVnemkapcsoltError(void);
void clearTeritoMVnemkapcsoltError(void);
int getTeritoMVnemkapcsoltError(void);

void setFelh1MVnemkapcsoltError(void);
void clearFelh1MVnemkapcsoltError(void);
int getFelh1MVnemkapcsoltError(void);

void setFelh2MVnemkapcsoltError(void);
void clearFelh2MVnemkapcsoltError(void);
int getFelh2MVnemkapcsoltError(void);

void setSalakMVnemkapcsoltError(void);
void clearSalakMVnemkapcsoltError(void);
int getSalakMVnemkapcsoltError(void);
//
void setAjtoProbalError(void);
void clearAjtoProbalError(void);
int getAjtoProbalError(void);

void setAjtoNemnyitottError(void);
void clearAjtoNemnyitottError(void);
int getAjtoNemnyitottError(void);

void setAjtoNemzartError(void);
void clearAjtoNemzartError(void);
int getAjtoNemzartError(void);

void setAjtoNyitvakapcsoloRossz(void);
void clearAjtoNyitvakapcsoloRossz(void);
int getAjtoNyitvakapcsoloRossz(void);

void setAjtoZarvakapcsoloRossz(void);
void clearAjtoZarvakapcsoloRossz(void);
int getAjtoZarvakapcsoloRossz(void);

void setSzintjelzoHiba(void);
void clearSzintjelzoHiba(void);
int getSzintjelzoHiba(void);

void setIdotullepesHiba(void);
void clearIdotullepesHiba(void);
int getIdotullepesHiba(void);

void setPockoloProbalError(void);
void clearPockoloProbalError(void);
int getPockoloProbalError(void);

void setPernyeProbalError(void);
void clearPernyeProbalError(void);
int getPernyeProbalError(void);

void setErrorSalakajtoNyitva(void);
void clearErrorSalakajtoNyitva(void);
int getErrorSalakajtoNyitva(void);

void setVedoberendezesekError(void);
void clearVedoberendezesekError(void);
int getVedoberendezesekError(void);

void setBovitoAramszunetError(void);
void clearBovitoAramszunetError(void);
int getBovitoAramszunetError(void);

void setKulsoTartalyUresError(void);
void clearKulsoTartalyUresError(void);
int getKulsoTartalyUresError(void);

// SMS hib�k

void setSMSaramszunet(void);
void clearSMSaramszunet(void);
void setSMSvesztermosztat(void);
void clearSMSvesztermosztat(void);
void setSMSfeleges(void);
void clearSMSfeleges(void);
void setSMSurestartaly(void);
void clearSMSurestartaly(void);
void setSMSvizszintalacsony(void);
void clearSMSvizszintalacsony(void);
void setSMStuzkialudt(void);
void clearSMStuzkialudt(void);
void setSMStulmelegedes(void);
void clearSMStulmelegedes(void);
void setSMStulfutes(void);
void clearSMStulfutes(void);
void setSMStoltohiba(void);
void clearSMStoltohiba(void);

void setSMSajtonyitva(void);
void clearSMSajtonyitva(void);
void setSMSsalakolohiba(void);
void clearSMSsalakolohiba(void);
void setSMSekletrahiba(void);
void clearSMSekletrahiba(void);
void setSMSstopError(void);
void clearSMSstopError(void);
void setSMSerror35(void);
void clearSMSerror35(void);
void setSMSerror41(void);
void clearSMSerror41(void);

int getToltohiba(void);
int getSalakolohiba(void);
int getEkletrahiba(void);

unsigned char get_relay_state(int idx);
int getTabletBit(void);
void setTabletBit(int bit);

#endif
