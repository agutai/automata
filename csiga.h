#ifndef __CSIGA_H
#define __CSIGA_H

#define CSIGA_NOT_OK			0
#define	CSIGA_OK				1
#define	CSIGA_PROBALKOZIK		2
#define	CSIGA_ELAKADT			3
#define MV_LEKAPCSOLT			4
#define MV_NEMKAPCSOLT_VISSZA	5

#define START_SZUNET			10
#define ELAKADASMENTES			30
#define HATRAMENET				10
#define	MOTORVEDO_IDO			600


typedef struct csiga_
{
 	int forgTimer;
	int felfutTimer;
	int motorvedoTimer;
	int proba_counter;
	int mehet;
	int error_state;
	int start;
	int kezistart;
	int motorvedo_start;
	int forg_state;
	int felfutas;
	int elakadt;
	int probalkozik;
	int forgido_mul;
	int is_mv;
	int MV;
	int MV_nemkapcsolt;
	int	aramfigyeles;
	int aramkorlat_lekapcsolt;

	int elore_pin;
	int hatra_pin;
	int forg_pin;
	int mv_pin;

	void (*Elore)(void);
	void (*Hatra)(void);
	void (*Allj)(void);

	int (*Motorvedo)(void);
	int (*Forgasfigyelo)(void);

//	int (*get_felfutas)(void);
	int (*get_forgido)(void);

	int (*get_aramkorlat)(void);
	int (*get_arammero)(void);

	int (*get_eloregomb)(void);
	int (*get_hatragomb)(void);

} csiga_;

void csigaTimer(volatile csiga_ *cst);
int csiga(volatile csiga_ *cs);

#endif
