#include "AT91SAM7X256.H"
#include "Board.h"
#include "data.h"


int outputs; 
int expander_inputs;
int expander_outputs; 

void wait(void)
{
	int x;
	for(x=0;x<100;x++);
}

void shift(void)
{
	int i;

	e_h597_reset_lo;wait();e_h597_reset_hi;wait();
	e_h597_st_clk_hi;wait();e_h597_st_clk_lo;wait();
	e_h597_pl_lo;wait();e_h597_pl_hi;wait();

	for(i=0;i<24;i++)
	{
		e_h597_sh_clk_hi;wait();
		if(e_h597_data > 0){expander_inputs |= (1 << i);}
		else {expander_inputs &= ~(1 << i);}
		wait();
		e_h597_sh_clk_lo;
		wait();
	}

	e_h595_reset_lo;wait();e_h595_reset_hi;wait();

	for(i=0;i<24;i++)
	{
		if((expander_outputs & (1<<i)) > 0){e_h595_data_hi;}
		else {e_h595_data_lo;}
		wait();
		e_h595_sh_clk_hi;wait(); e_h595_sh_clk_lo;wait();
	}

	e_h595_st_clk_hi;wait();e_h595_st_clk_lo;wait();
	//Internal shift
	i_h595_reset_lo;wait();i_h595_reset_hi;wait();

	for(i=0;i<16;i++)
	{
		if((outputs & (1<<i)) > 0){i_h595_data_hi;}
		else {i_h595_data_lo;}
		wait();
		i_h595_sh_clk_hi;wait(); i_h595_sh_clk_lo;wait();
	}

	i_h595_st_clk_hi;wait();i_h595_st_clk_lo;wait();

} //Shift


void set_outputs(int out)
{
 	outputs |= (1 << out);
}

void clear_outputs(int out)
{
	outputs &= ~(1 << out);
}

//B�v�t�
int get_expander_input(int in)
{
	if((expander_inputs & (1<<in)) > 0){return 1;}
		else return 0;
}

void set_expander_outputs(int out)
{
	expander_outputs |= (1 << out);
}

void clear_expander_outputs(int out)
{
	expander_outputs &= ~(1 << out);
}
