

#ifndef __TOLTO_H
#define __TOLTO_H

void Tolto(void);
void Salakolo(void);

void toltoInit(void);
void salakInit(void);

void setImitidoTimer(int);
int getImitidoTimer(void);

void setSalakszunetTimer(int);
int getSalakszunetTimer(void);

int getSalakol(void);

void toltoTenthTimer(void);
void toltoSecTimer(void);
void toltoMinTimer(void);

void salakTenthTimer(void);
void salakSecTimer(void);
void salakMinTimer(void);

#endif
