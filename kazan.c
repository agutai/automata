#include "AT91SAM7X256.H"
#include "kazan.h"
#include "Board.h"
#include "shift.h"
#include "data.h"
#include "expander.h"
#include "Tolto.h"

volatile int KazanMehet;
volatile int rostely_be, rostely_szunet;
volatile int rostely_timer,bolygato_timer,timer_on_timer,timer_szunet_timer;
volatile int szivattyu_timer,szivattyu_timer_start;
volatile int rostely_timer_start,rostelymegy;
volatile int bolygato_timer_start,bolygatomegy;
volatile int bolyg_ido;
volatile int UzemiBolygatas,AlloBolygatas,VeszBolygatas;
volatile int kialvasgatlo_start, kialvasgatlo_be;
volatile int visszahutes;
volatile int kihules_timer, kihules_timer_start;
volatile int tuzoltas_timer, tuzoltas_start;

volatile int kulso_szivattyu_tilt;

void kazanTenthTimer(void)
{
	rostely_timer++;
}

void kazanSecTimer(void)
{
	bolygato_timer++;
	timer_on_timer++;
	szivattyu_timer++;
	kihules_timer++;
	tuzoltas_timer++;
}

void kazanMinTimer(void)
{
	timer_szunet_timer++;
}

void kazan_nem_gozos(void)
{
	if(!getAramszunet() && !VESZTERMOSZTAT && (getHomero1() < getVizhoMax()) )
	{
		if(getMelegentart())
		{
			if(getHomero1() >= getHofok1())				//Kikapcs ha felf�t�tt
			{
				KazanMehet = 0;
			}
			if(getHomero1() < (getHofok1()-getVizhoHyst()))		//Bekapcs ha leh�lt
			{
				KazanMehet = 1;
			}
		}
		else
		{
			if(SZOBATERMOSZTAT)
			{
				if(getHomero1() >= getHofok1())				//Kikapcs ha felf�t�tt
				{
					KazanMehet = 0;
				}
				if(getHomero1() < (getHofok1()-getVizhoHyst()))		//Bekapcs ha leh�lt
				{
					KazanMehet = 1;
				}
			}
			else
			{
				KazanMehet = 0;
			}
		}

		if(kialvasgatlo_be)		//Kialv�sg�tl�s
		{
			if(getHomero1() >= getHofok1())				//Kikapcs ha felf�t�tt
			{
				KazanMehet = 0;
			}
			if(getHomero1() < (getHofok1()-getVizhoHyst()))		//Bekapcs ha leh�lt
			{
				KazanMehet = 1;
			}
		}
	}
	else
	{
		KazanMehet = 0;
	}
}

void kazan_gozos(void)
{
	if(!getAramszunet() && !VESZTERMOSZTAT )
	{
		if(getGozNyomasmero() >= getGoznyomas())			//Kikapcs ha felf�t�tt
		{
			KazanMehet = 0;
		}
		if(getGozNyomasmero() < (getGoznyomas()-getFelsoNyomasHyst()))	//Bekapcs ha leh�lt
		{
			KazanMehet = 1;
		}

		if(kialvasgatlo_be)
		{
			if(getGozNyomasmero() >= getGoznyomas()) 			//Kikapcs ha felf�t�tt
			{
				KazanMehet = 0;
			}
			if(getGozNyomasmero() < (getGoznyomas()-getFelsoNyomasHyst()))	//Bekapcs ha leh�lt
			{
				KazanMehet = 1;
			}
		}

		if(!FELSO_SZINT && !getErrorVizszintAlacsony() )
		{
			SZIVATTYU_ON;
		}
		else
		{
			SZIVATTYU_OFF;
			clearGozfelkeszul();
		}

		if(getGozfelkeszul() || getErrorVizszintAlacsony())
		{
			KazanMehet = 0;
		}

		if(!getGozfelkeszul() && !ALSO_SZINT)
		{
			setErrorVizszintAlacsony();
		}

	}
	else
	{
		KazanMehet = 0;
	}
}

void kazan_kezi(void)
{
	if(getRostelyGomb() > 0){ROSTELY_ON;rostelymegy = 1;}
	else{ROSTELY_OFF;rostelymegy = 0;}

	if(getVentiGomb() > 0){VENTI_ON;}
	else{VENTI_OFF;}

	if(getBolygGomb() > 0){BOLYG_ON;bolygatomegy = 1;}
	else{BOLYG_OFF;bolygatomegy = 0;}

	if(getBelimoGomb() > 0){BELIMO_ON;}
	else{BELIMO_OFF;}

	if(getSzivattyuGomb() > 0){SZIVATTYU_ON;}
	else{SZIVATTYU_OFF;}

	if(getSzivattyu2Gomb() > 0){SZIVATTYU2_ON;}
	else{SZIVATTYU2_OFF;}
}

void kazan_automata(void)
{
	if(KazanMehet == 1)
	{
		szivattyu_timer = 0;
		szivattyu_timer_start = 1;
		if(rostelymegy == 0)
		{
			if(rostely_timer_start == 0)	//Rost�ly ind�t�sa
			{
				rostely_timer = 0;
				rostely_timer_start = 1;
			}

			if(rostely_timer > rostely_szunet)
			{
				if((rostely_be > 0) && (getFauzem() == 0) && (getKazanType()==1))
				{
					ROSTELY_ON;
				}
				rostelymegy = 1;
				rostely_timer_start = 0;
			}
		}
	}

	if(rostelymegy == 1)
	{
		if(rostely_timer_start == 0)		//Rost�ly meg�ll�t�sa
		{
			rostely_timer = 0;
			rostely_timer_start = 1;
		}

		if(rostely_timer > rostely_be)
		{
			ROSTELY_OFF;
			rostelymegy = 0;
			rostely_timer_start = 0;
		}	
	}

	if((KazanMehet == 1) && !FELEGES && (getFauzem() == 0))		//�zemi bolygat�s
	{
		bolyg_ido = getUzemiBolyg() * 10;
	}

	if(((KazanMehet == 0) || (getFauzem() > 0)) && !FELEGES)	//�ll�helyzeti bolygat�s
	{
		bolyg_ido = getAlloBolyg() * 60;
	}

	if(bolygatomegy == 0)
	{
		if(bolygato_timer_start == 0)		//Bolygat� ind�t�sa
		{
			bolygato_timer = 0;
			bolygato_timer_start = 1;
		}

		if(bolygato_timer >= bolyg_ido)
		{
			if(rostely_be > 0)		//0 sebess�gn�l bolygat� meg�ll�t�sa
			{
				BOLYG_ON;
			}
			bolygatomegy = 1;
			bolygato_timer_start = 0;
		}
	}
        
	if(bolygatomegy == 1)
	{
		if(bolygato_timer_start == 0)		//Bolygat� meg�ll�t�sa
		{
			bolygato_timer = 0;
			bolygato_timer_start = 1;
		}

		if(bolygato_timer >= getBolygBe())
		{
			BOLYG_OFF;
			bolygatomegy = 0;
			bolygato_timer_start = 0;
		}	
	}

	if(KazanMehet == 1)
	{ 
		VENTI_ON;

		if(!getGozos())
		{
			BELIMO_ON;
		}
		else
		{
			if(getGozNyomasmero() > (getGoznyomas() - getAlsoNyomasTav()))
			{
				BELIMO_OFF;
			}

			if(getGozNyomasmero() < (getGoznyomas() - getAlsoNyomasTav()))
			{
				BELIMO_ON;
			}
		}
	}

	if(KazanMehet == 0)
	{
		VENTI_OFF;
		if(AJTO_NYITVA)
		{
			BELIMO_ON;
		}
		else
		{
			BELIMO_OFF;
		}
	}

	if(!getGozos())
	{
		kazan_nem_gozos();
	}
	else
	{
		kazan_gozos();
	}

	if(FELEGES || (FELEGES_FELSO && getFelsofelegesPresent()))	//V�szbolygat�s
	{
		if(getBurninEnable() == 0)
		{
			KazanMehet = 0;
		}
		bolyg_ido = 30;
	}

#if (1)
	if(FELEGES_FELSO && getFelsofelegesPresent())			//T�zolt�s
	{
		if(!tuzoltas_start)
		{
			tuzoltas_start = 1;
			tuzoltas_timer = 0;
			TUZOLTAS_ON;
		}	
	}
	else
	{
		TUZOLTAS_OFF;
		tuzoltas_start = 0;
	}

	if(tuzoltas_start && (tuzoltas_timer > (getTuzoltasTime() * 60)))
	{
		TUZOLTAS_OFF;
	}
#endif

	if(TARTALY_URES)
	{
		if(getLowlevelEnable() == 0)		//Ha �res a tart�ly, mehet-e?
		{
			KazanMehet = 0;
		}
	}

	if((getSalakol() && getSalakoloInstalled()) || (!AJTO_CSUKVA && getToltoInstalled()) || (FELEGES_FELSO && getFelsofelegesPresent()))
	{
		KazanMehet = 0;
		kulso_szivattyu_tilt = 1;
	}
	else
	{
		kulso_szivattyu_tilt = 0;
	}

	if(!VENTI_MV && BOVITO_LEKAPCS)
	{
		setVentilatorMVError();
	}
	else
	{
		clearVentilatorMVError();
	}

	if(!getGozos())
	{
		if(SZOBATERMOSZTAT || visszahutes || ((KazanMehet == 1) && (getRoomTherPresent() == 0)))
		{
			SZIVATTYU_ON;
		}
		else
		{
			SZIVATTYU_OFF;
		}

		if((getHomero1() >= (getHofok1() + getTulfutHyst())) || (getHomero1() >= getFigyelmeztetesiHofok()))
		{
			visszahutes = 1;
			setTulfutes();
		}
		if(visszahutes && ( getHomero1() <= getHofok1()))
		{
			visszahutes = 0;
			clearTulfutes();
		}
	}

	if(kulso_szivattyu_tilt && !visszahutes)
	{
		SZIVATTYU_TILTAS_ON;
	}
	else
	{
		SZIVATTYU_TILTAS_OFF;
	}
}

void kazan(void)
{  
	// Rost�lysebess�g kisz�m�t�sa
	if (getRostelyseb() == 0)
	{
		rostely_szunet = 0;
		rostely_be = 0;
	}
	else
	{
		double x;
		rostely_be = 50;
		x = (double)getRostelyseb();
		x = 100 / x;
		x = x - 1;
		x *= 50;
		rostely_szunet = (int)x;
	}

	if(getRelAGomb() > 0){RELA_ON;}
	else{RELA_OFF;}

	if(getRelBGomb() > 0){RELB_ON;}
	else{RELB_OFF;}

	if(getKezi())
	{
		kazan_kezi();
	}
	else
	{
		kazan_automata();
	}
    
	if(getKihultError() || getStopError() || (getError35Stop() && getError35()))
	{
		KazanMehet = 0;
	}

	if(getDemo())
	{
		KazanMehet = 0;
	}

	if(KazanMehet == 1)
	{
		timer_szunet_timer = 0;
	}

	if(timer_szunet_timer > (getTimerSzunet()*60))
	{
		if(kialvasgatlo_start == 0)
		{
			kialvasgatlo_start = 1;
			timer_on_timer = 0;
			kialvasgatlo_be = 1;
		}
	}

	if(kialvasgatlo_be == 1)
	{
		if(timer_on_timer > (getTimerBe()*60))
		{
			kialvasgatlo_start = 0;
			kialvasgatlo_be = 0;
		}
	}

	if((KazanMehet == 1) && (rostely_be > 0))
	{
		if(kihules_timer_start == 0)		//Id�z�t� ind�t�sa
		{
			kihules_timer_start = 1;
			kihules_timer = 0;
		}
	
		if(kihules_timer >= (getKihulIdo() * 60))
		{
			if(getHomero2() < getKihulHyst())
			{
				if(getFusthomeroPresent())
				{
					setKihultError();
				}
			}
		}
	}
	else
	{
		kihules_timer_start = 0;
	}

	if(getClearGomb())
	{
		clearStopError();
		clearKihultError();
		clearError35();
		kihules_timer_start = 0;
		if(getErrorVizszintAlacsony())
		{
			setGozfelkeszul();
			clearErrorVizszintAlacsony();
		}
	}

	if(!getGozos())
	{
		if((getHomero1() > getFigyelmeztetesiHofok()) )
		{
			setOverheatError();
			KazanMehet = 0;
		}
		else
		{
			clearOverheatError();
		}
		
		if(getTulfutes() && getTulfutesAlarm())
		{
			setSMStulfutes();
		}
		else
		{
			clearSMStulfutes();
		}
	}

	if(FELEGES_FELSO && getFelsofelegesPresent())
	{
		setTuzoltasError();
	}
	else
	{
		clearTuzoltasError();
	}
}
