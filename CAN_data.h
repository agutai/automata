#ifndef __CAN_DATA_H
#define __CAN_DATA_H

void setCANrx_data(int ID, unsigned char[]);
void setCANtx_data(int page, unsigned char[]);
void setCAN70tx_data(int page, unsigned char[]);
void refreshCANdata(void);
void refreshCAN70data(void);

void setCANGyujtocsigaMehet(void);
void clearCANGyujtocsigaMehet(void);
int getCANGyujtocsigaMehet(void);

void setCANEkletraMehet(void);
void clearCANEkletraMehet(void);
int getCANEkletraMehet(void);

void setCANSalakoloMehet(void);
void clearCANSalakoloMehet(void);
int getCANSalakoloMehet(void);

void setCANGyujtottHiba(void);
void clearCANGyujtottHiba(void);
int getCANGyujtottHiba(void);

void setCANAramszunet(void);
void clearCANAramszunet(void);
int getCANAramszunet(void);
void setCANVesztermosztat(void);
void clearCANVesztermosztat(void);
int getCANVesztermosztat(void);
void setCANKazanKihult(void);
void clearCANKazanKihult(void);
int getCANKazanKihult(void);
void setCANVizszintAlacsony(void);
void clearCANVizszintAlacsony(void);
int getCANVizszintAlacsony(void);
void setCANToltoHiba(void);
void clearCANToltoHiba(void);
int getCANToltoHiba(void);
void setCANToltoajtoNemzart(void);
void clearCANToltoajtoNemzart(void);
int getCANToltoajtoNemzart(void);
void setCANSalakoloHiba(void);
void clearCANSalakoloHiba(void);
int getCANSalakoloHiba(void);

int getCANHidraulikaNyomas(void);
int getCANSalakoloOK(void);

// CAN70

void setCAN70EkletraMehet(void);
void clearCAN70EkletraMehet(void);
int getCAN70EkletraMehet(void);
void setCAN70GyujtocsigaMehet(void);
void clearCAN70GyujtocsigaMehet(void);
int getCAN70GyujtocsigaMehet(void);

void setCAN70EkletraTilt(void);
void clearCAN70EkletraTilt(void);
int getCAN70EkletraTilt(void);
void setCAN70Csiga1Tilt(void);
void clearCAN70Csiga1Tilt(void);
int getCAN70Csiga1Tilt(void);

int getCAN70error(void);
int getCAN70errorEkletraIdotullepes(void);
int getCAN70errorCsiga1Probal(void);
int getCAN70errorCsiga1Elakadt(void);
int getCAN70BoilerStop(void);
int getCAN70inputs(void);
int getCAN70outputs(void);

// CAN71
int getCAN71BoilerNum(void);
int getCAN71SetBit(void);
int getCAN71StopBit(void);
int getCAN71PresetTemp(void);
int getCAN71PresetBar(void);


#endif
